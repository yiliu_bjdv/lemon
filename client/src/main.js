/**
 * @fileOverview 主程序
 * @author wing ying_gong@bjdv.com
 */
'use strict';

require('./modules/ui-load.js');
require('./modules/ui-jq.js');

angular.module('app', [
    'ngAnimate',
    'ngCookies',
    'ngSanitize',
    'ngTouch',
    'ngStorage',
    'ngFileUpload',
    'ui.router',
    'ui.bootstrap',
    'ui.utils',
    'ui.load',
    'ui.jq',
    'ui.tree',
    'selectize',
    'oc.lazyLoad',
    'pascalprecht.translate',
    'toaster',
    'ui.ace'
]);

require('./config');
require('./config.lazyload');
require('./config.router');

/** filter */
require('./filters/filter.js');
/** service */
require('./services/service.js');
require('./services/app_service');
require('./services/socket.js');
/** directive */
require('./directives/setnganimate.js');
require('./directives/ui-butterbar.js');
require('./directives/ui-focus.js');
require('./directives/ui-fullscreen.js');
require('./directives/ui-module.js');
require('./directives/ui-nav.js');
require('./directives/ui-scroll.js');
require('./directives/ui-shift.js');
require('./directives/ui-toggleclass.js');

require('./directives/godiagram.js');
require('./directives/datetimepicker.js');
require('./directives/barcode.js');
require('./directives/qrcode.js');
require('./directives/upload.js');
require('./directives/image.js');
require('./directives/confirm.js');

require('./directives/app_directive.js');

/** models */
require('./models/model.js');

/** controller */
require('./controllers/app.js');
require('./controllers/sign.js');

require('./controllers/global/permission.js');
require('./controllers/global/global_meta.js');
require('./controllers/global/global_config.js');
require('./controllers/global/tenant.js');

require('./controllers/system/metadata.js');
require('./controllers/system/config.js');
require('./controllers/system/department.js');
require('./controllers/system/user.js');

require('./controllers/service/service.js');
require('./controllers/service/service_mxb.js');
require('./controllers/service/service_axb.js');
require('./controllers/service/service_template.js');

require('./controllers/business/customer.js');