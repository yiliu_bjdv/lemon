/**
 * @fileOverview 全局字典操作
 * @author wing ying_gong@bjdv.com
 */
'use strict';
var app = angular.module('app');
app.controller('GlobalMetaCtrl', function ($scope, $state, $notify, $uibModal, $global_meta) {
    $scope.page = { limit: 10 };
    $scope.cond = {};
    $global_meta.categories().then(function (r) {
        $scope.categories = r;
    }).catch(function (e) {
        $notify.warning('全局字典', e.message);
    });

    $scope.search = function (page) {
        $global_meta.findAllPage({ key: $scope.cond.selectedKey }, page, $scope.page.limit).then(function (r) {
            $scope.global_metas = r.rows;
            $scope.page.gage = r.page;
            $scope.page.total = r.total;
            $scope.page.limit = r.limit;
            $scope.page.count = r.count;
        }).catch(function (e) {
            $notify.warning('全局字典', e.message);
        });
    };

    $scope.add = function () {
        var selectedKey = $scope.cond.selectedKey;
        $uibModal.open({
            size: 'sm',
            templateUrl: 'views/global/global_meta_add.html',
            controller: 'GlobalMetaAddCtrl',
            resolve: {
                key: function () {
                    return selectedKey;
                }
            }
        }).result.then(function () {
            $scope.search();
        }, function () {
            $scope.search();
        });
    };

    $scope.edit = function (item) {
        $uibModal.open({
            size: 'sm',
            templateUrl: 'views/global/global_meta_edit.html',
            controller: 'GlobalMetaEditCtrl',
            resolve: {
                item: function () {
                    return item;
                }
            }
        }).result.then(function () {
            $scope.search();
        }, function () {
            $scope.search();
        });
    };

    $scope.remove = function (item) {
        $global_meta.remove(item.id).then(function () {
            $notify.info('全局字典', '删除成功');
            $scope.search();
        }).catch(function (e) {
            $notify.info('全局字典', e.message);
            $scope.search();
        });
    };

    $scope.$watch('cond.selectedKey', function () {
        if (!_.isUndefined($scope.cond.selectedKey)) {
            $scope.search();
        }
    });
});

app.controller('GlobalMetaAddCtrl', function ($scope, $uibModalInstance, $notify, key, $global_meta) {
    if(key=='product:sub_category' || key=='product:category'){
        $global_meta.findAll({status: 1}, [], ['context', 'value','key']).then(function(r){
            $scope.metadatas = r;
        });
        if(key=='product:category'){
            $scope.meta = { key: key, status: 1 ,ref_key:'product:type'};
        }else{
            $scope.meta = { key: key, status: 1 ,ref_key:'product:category'};
        }
    }else{
        $scope.meta = { key: key, status: 1 };
    }
    

    $scope.save = function () {
        $global_meta.create($scope.meta).then(function () {
            $notify.info('全局字典', '保存成功');
            $uibModalInstance.close();
        }).catch(function (e) {
            $notify.warning('全局字典', e.message);
        });
    };

    $scope.cancel = function () {
        $uibModalInstance.dismiss();
    };
});

app.controller('GlobalMetaEditCtrl', function ($scope, $uibModalInstance, $notify, item, $global_meta) {
    $scope.meta = item;
    if(item.key=='product:sub_category' || item.key=='product:category'){
        $global_meta.findAll({status: 1}, [], ['context', 'value','key']).then(function(r){
                $scope.metadatas = r;
        });
    }
    

    $scope.save = function () {
        $global_meta.save(item.id, $scope.meta).then(function () {
            $notify.info('全局字典', '保存成功');
            $uibModalInstance.close();
        }).catch(function (e) {
            $notify.warning('全局字典', e.message);
        });
    };

    $scope.cancel = function () {
        $uibModalInstance.dismiss();
    };
});