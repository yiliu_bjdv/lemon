/**
 * @fileOverview 全局配置操作
 * @author wing ying_gong@bjdv.com
 */
'use strict';
var app = angular.module('app');
app.controller('GlobalConfigCtrl', function ($scope, $state, $notify, $uibModal, $global_config) {
    $scope.page = { limit: 10 };
    $scope.cond = {};
    $global_config.categories().then(function (r) {
        $scope.categories = r;
    });

    $scope.add = function () {
        $uibModal.open({
            size: 'sm',
            templateUrl: 'global_config_add.html',
            controller: 'GlobalConfigAddCtrl',
            resolve: {
                key: function () {
                    return $scope.cond.selectedKey;
                }
            }
        }).result.then(function () {
            $scope.search();
        }, function () {
            $scope.search();
        });
    };
    $scope.edit = function (item) {
        $uibModal.open({
            size: 'sm',
            templateUrl: 'global_config_edit.html',
            controller: 'GlobalConfigEditCtrl',
            resolve: {
                config: function () {
                    return item;
                }
            }
        }).result.then(function () {
            $scope.search();
        }, function () {
            $scope.search();
        });
    };

    $scope.remove = function (item) {
        $global_config.remove(item.id).then(function () {
            $notify.info('配置', '删除成功');
            $scope.search();
        }).catch(function (e) {
            $notify.warning('配置', e.message);
        });
    };

    $scope.search = function (page) {
        $global_config.findAllPage({ key: $scope.cond.key }, page, $scope.page.limit).then(function (r) {
            $scope.configs = r.rows;
            $scope.page.total = r.total;
            $scope.page.page = r.page;
            $scope.page.limit = r.limit;
            $scope.page.count = r.count;
        });
    };
    $scope.search();
});

app.controller('GlobalConfigAddCtrl', function ($scope, $notify, $uibModalInstance, key, $global_config) {
    $scope.config = { key: key, status: 1 };
    $scope.save = function () {
        $global_config.upsert($scope.config, {key:$scope.config.key}).then(function () {
            $notify.info('配置', '保存成功');
            $uibModalInstance.close();
        });
    };
    $scope.cancel = function () {
        $uibModalInstance.dismiss();
    };
});

app.controller('GlobalConfigEditCtrl', function ($scope, $notify, $uibModalInstance, config, $global_config) {
    $scope.config = config;
    $scope.save = function () {
        $global_config.save($scope.config.id, $scope.config).then(function () {
            $notify.info('配置', '保存成功');
            $uibModalInstance.close();
        });
    };
    $scope.cancel = function () {
        $uibModalInstance.dismiss();
    };
});