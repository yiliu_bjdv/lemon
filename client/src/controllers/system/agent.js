/**
 * @fileoverview 坐席操作
 * @author wing ying_gong@bjdv.com
 */
'use strict';
var app = angular.module('app');
app.controller('AgentCtrl', function ($scope, $state, $notify, $uibModal, $agent) {
    $scope.page = { limit: 10 };
    $scope.search = function (page) {
        page = page || $scope.page.page;
        var where;
        if($scope.info==undefined || $scope.info==''){
            where = {};
        }else{
            where ={ $or: [{ code: { $like: '%' + $scope.info + '%' } }, { name: { $like: '%' + $scope.info + '%' } }] };
        }
        $agent.findAllPage(where, page, $scope.page.limit, ['id']).then(function (r) {
            $scope.agents = r.rows;
            $scope.page.page = r.page;
            $scope.page.limit = r.limit;
            $scope.page.count = r.count;
            $scope.page.total = r.total;
        });
    };

    $scope.search();

    $scope.add = function () {
        $uibModal.open({
            size: 'lg',
            controller: 'AgentAddCtrl',
            templateUrl: 'views/system/agent_add.html',
        }).result.then(function () {
            $scope.search();
        }, function () {
            $scope.search();
        });
    };

    $scope.edit = function (agent) {
        $uibModal.open({
            size: 'lg',
            controller: 'AgentEditCtrl',
            templateUrl: 'views/system/agent_edit.html',
            resolve: {
                agent: function () {
                    return agent;
                }
            }
        }).result.then(function () {
            $scope.search();
        }, function () {
            $scope.search();
        });
    };

    $scope.remove = function (agent) {
        $agent.remove(agent.id).then(function () {
            $notify.info('坐席', '删除成功');
            $scope.search();
        }).catch(function (e) {
            $notify.warning('坐席', e.message);
            $scope.search();
        });
    };
});

app.controller('AgentAddCtrl', function ($scope, $state, $notify, $uibModalInstance, $agent, $permission, $queue) {
    $scope.agent = {};
    $permission.findAll({}, [], ['id', 'name']).then(function (rows) {
        $scope.permissions = rows;
    });
    $queue.findAll({ 'status': 1 }, [], ['id', 'name']).then(function (rows) {
        $scope.queues = rows;
    });
    $scope.save = function () {
        $agent.password = $.md5($scope.agent.password);
        $agent.create($scope.agent).then(function () {
            $notify.info('修改坐席', '成功');
            $uibModalInstance.close();
        }).catch(function (e) {
            $notify.warning('修改坐席', e.message);
            $uibModalInstance.close();
        });
    };

    $scope.cancel = function () {
        $uibModalInstance.dismiss();
    };
});

app.controller('AgentEditCtrl', function ($scope, $state, $notify, $uibModalInstance, agent, $agent, $permission, $queue) {
    $scope.agent = agent;
    $permission.findAll({}, [], ['id', 'name']).then(function (rows) {
        $scope.permissions = rows;
    });
    $queue.findAll({ 'status': 1 }, [], ['id', 'name']).then(function (rows) {
        $scope.queues = rows;
    });

    $scope.save = function () {
        if ($scope.agent.new_password) {
            $agent.password = $.md5($scope.agent.new_password);
        }
        $agent.save($scope.agent.id, $scope.agent).then(function () {
            $notify.info('修改坐席', '成功');
            $uibModalInstance.close();
        }).catch(function (e) {
            $notify.warning('修改坐席', e.message);
            $uibModalInstance.close();
        });
    };

    $scope.cancel = function () {
        $uibModalInstance.dismiss();
    };
});

app.controller('ProfileCtrl', function ($scope, $state, $notify, $agent, UserService, $localStorage) {
   
    UserService.getUser().then(function (user) {
        $scope.agent = user;
        return $agent.findOne(user.id);
    }).then(function(row){
        $scope.oriPassword = row.password;
    });

    
    $scope.save = function () {        
        if ($scope.oriPassword != $.md5($scope.agent.password)) {
            $notify.warning('修改密码', '原始密码错误');
            return;
        }
        if ($scope.agent.new_password != $scope.agent.new_password2) {
            $notify.warning('修改密码', '两次密码输入不一致');
            return;
        } else {
            $agent.password = $.md5($scope.agent.new_password);
        }
        $agent.save($scope.agent.id, $scope.agent).then(function () {
            $notify.info('修改密码', '成功');
            $scope.logout();
        }).catch(function (e) {
            $notify.warning('修改密码', e.message);
        });
    };

    $scope.logout = function () {
        delete $localStorage.token;
        delete $localStorage.user;
        $state.go('access.signin');
    };
});

app.controller('AgentDetailCtrl', function ($scope, $state, $notify, $uibModal, $agent_detail) {
    $scope.page = { limit: 10 };
    $scope.search = function (page) {
        page = page || $scope.page.page;
        var where;
        if($scope.info==undefined || $scope.info==''){
            where = {};
        }else{
            where ={ $or: [{ alis: { $like: '%' + $scope.info + '%' } }, { name: { $like: '%' + $scope.info + '%' } }] };
        }
        $agent_detail.findAllPage(where, page, $scope.page.limit, ['id']).then(function (r) {
            $scope.agents = r.rows;
            $scope.page.page = r.page;
            $scope.page.limit = r.limit;
            $scope.page.count = r.count;
            $scope.page.total = r.total;
        });
    };

    $scope.search();


});
