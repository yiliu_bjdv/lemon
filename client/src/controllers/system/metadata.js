/**
 * @fileOverview 数据字典操作
 * @author wing ying_gong@bjdv.com
 */
'use strict';
var app = angular.module('app');
app.controller('MetadataCtrl', function ($scope, $state, $notify, $uibModal, $metadata) {
    $scope.page = { limit: 10 };
    $scope.cond = {};

    $metadata.categories().then(function (r) {
        $scope.categories = r;
    });

    $scope.add = function () {
        $uibModal.open({
            size: 'sm',
            templateUrl: 'metadata_add.html',
            controller: 'MetadataAddCtrl',
            resolve: {
                key: function () {
                    return $scope.cond.selectedKey;
                }
            }
        }).result.then(function () {
            $scope.search();
        }, function () {
            $scope.search();
        });
    };

    $scope.edit = function (metadata) {
        $uibModal.open({
            size: 'sm',
            templateUrl: 'metadata_edit.html',
            controller: "MetadataEditCtrl",
            resolve: {
                metadata: function () {
                    return metadata;
                }
            }
        }).result.then(function () {
            $scope.search();
        }, function () {
            $scope.search();
        });
    };

    $scope.remove = function (metadata) {
        $metadata.remove(metadata.id).then(function () {
            $notify.info('数据源删除', '成功');
            $scope.search();
        }).catch(function (e) {
            $notify.warning('数据源删除', e.message);
            $scope.search();
        });
    };

    $scope.search = function (page) {
        var where = {};
        where.key = $scope.cond.selectedKey;
        $metadata.findAllPage(where, page, $scope.page.limit).then(function (r) {
            $scope.page.page = r.page;
            $scope.page.total = r.total;
            $scope.page.count = r.count;
            $scope.metadatas = r.rows;
        });
    };
    $scope.$watch('cond.selectedKey', function () {
        if (!_.isUndefined($scope.cond.selectedKey)) {
            $scope.search();
        }
    });
});

app.controller('MetadataAddCtrl', function ($scope, $uibModalInstance, $notify, key, $metadata) {
    $scope.metadata = { key: key, status: 1 };
    $scope.save = function () {
        $metadata.create($scope.metadata).then(function () {
            $notify.info('添加数据源', '成功');
            $uibModalInstance.close();
        }).catch(function (e) {
            $notify.warning('添加数据源', e.message);
            $uibModalInstance.close();
        });
    };
    $scope.cancel = function () {
        $uibModalInstance.dismiss();
    };
});

app.controller('MetadataEditCtrl', function ($scope, $uibModalInstance, $notify, metadata, $metadata) {
    $scope.metadata = metadata;
    $scope.save = function () {
        $metadata.save($scope.metadata.id, $scope.metadata).then(function () {
            $notify.info('修改数据源', '成功');
            $uibModalInstance.close();
        }).catch(function (e) {
            $notify.warning('修改数据源', e.message);
            $uibModalInstance.close();
        });
    };
    $scope.cancel = function () {
        $uibModalInstance.dismiss();
    };
});