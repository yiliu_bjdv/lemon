/**
 * @fileoverview 坐席操作
 * @author wing ying_gong@bjdv.com
 */
'use strict';
var app = angular.module('app');
app.controller('UserCtrl', function ($scope, $state, $notify, $uibModal, $user) {
    $scope.cond = {};
    $scope.page = { limit: 10 };
    $scope.search = function (page) {
        page = page || $scope.page.page;
        var where;
        if(!$scope.cond.like){
            where = {};
        }else{
            where ={ $or: [ { name: { $like: '%' + $scope.cond.like + '%' } }] };
        }
        $user.findAllPage(where, page, $scope.page.limit, []).then(function (r) {
            $scope.users = r.rows;
            $scope.page.page = r.page;
            $scope.page.limit = r.limit;
            $scope.page.count = r.count;
            $scope.page.total = r.total;
        });
    };

    $scope.search();

    $scope.add = function () {
        $uibModal.open({
            size: 'lg',
            controller: 'UserAddCtrl',
            templateUrl: 'views/system/user_add.html',
        }).result.then(function () {
            $scope.search();
        }, function () {
            $scope.search();
        });
    };

    $scope.edit = function (user) {
        $uibModal.open({
            size: 'lg',
            controller: 'UserEditCtrl',
            templateUrl: 'views/system/user_edit.html',
            resolve: {
                user: function () {
                    return user;
                }
            }
        }).result.then(function () {
            $scope.search();
        }, function () {
            $scope.search();
        });
    };

    $scope.remove = function (user) {
        $user.remove(user.id).then(function () {
            $notify.info('坐席', '删除成功');
            $scope.search();
        }).catch(function (e) {
            $notify.warning('坐席', e.message);
            $scope.search();
        });
    };
});

app.controller('UserAddCtrl', function ($scope, $state, $notify, $uibModalInstance, $user, $level) {
    $scope.user = {};
    $scope.passwd = {};
    $level.findAll({}, [], ['id', 'name']).then(function (rows) {
        $scope.levels = rows;
    });
    $scope.save = function () {
        $user.password = $.md5($scope.passwd.password);
        $user.create($scope.user).then(function () {
            $notify.info('修改坐席', '成功');
            $uibModalInstance.close();
        }).catch(function (e) {
            $notify.warning('修改坐席', e.message);
            $uibModalInstance.close();
        });
    };

    $scope.cancel = function () {
        $uibModalInstance.dismiss();
    };
});

app.controller('UserEditCtrl', function ($scope, $state, $notify, $uibModalInstance, user, $user, $level) {
    
    $level.findAll({}, [], ['id','level', 'name']).then(function (rows) {
        $scope.levels = rows;
    }).then(function(){
        $scope.user = user;
        $scope.passwd = {};
    });
    $scope.save = function () {
        if ($scope.passwd.password) {
            $user.password = $.md5($scope.passwd.password);
        }
        $user.save($scope.user.id, $scope.user).then(function () {
            $notify.info('修改坐席', '成功');
            $uibModalInstance.close();
        }).catch(function (e) {
            $notify.warning('修改坐席', e.message);
            $uibModalInstance.close();
        });
    };

    $scope.cancel = function () {
        $uibModalInstance.dismiss();
    };
});

app.controller('ProfileCtrl', function ($scope, $state, $notify, $user, UserService, $localStorage) {
   
    UserService.getUser().then(function (user) {
        $scope.user = user;
        return $user.findOne(user.id);
    }).then(function(row){
        $scope.oriPassword = row.password;
    });

    
    $scope.save = function () {        
        if ($scope.oriPassword != $.md5($scope.user.password)) {
            $notify.warning('修改密码', '原始密码错误');
            return;
        }
        if ($scope.user.new_password != $scope.user.new_password2) {
            $notify.warning('修改密码', '两次密码输入不一致');
            return;
        } else {
            $user.password = $.md5($scope.user.new_password);
        }
        $user.save($scope.user.id, $scope.user).then(function () {
            $notify.info('修改密码', '成功');
            $scope.logout();
        }).catch(function (e) {
            $notify.warning('修改密码', e.message);
        });
    };

    $scope.logout = function () {
        delete $localStorage.token;
        delete $localStorage.user;
        $state.go('access.signin');
    };
});

app.controller('UserDetailCtrl', function ($scope, $state, $notify, $uibModal, $user_detail) {
    $scope.page = { limit: 10 };
    $scope.search = function (page) {
        page = page || $scope.page.page;
        var where;
        if($scope.info==undefined || $scope.info==''){
            where = {};
        }else{
            where ={ $or: [{ alis: { $like: '%' + $scope.info + '%' } }, { name: { $like: '%' + $scope.info + '%' } }] };
        }
        $user_detail.findAllPage(where, page, $scope.page.limit, ['id']).then(function (r) {
            $scope.users = r.rows;
            $scope.page.page = r.page;
            $scope.page.limit = r.limit;
            $scope.page.count = r.count;
            $scope.page.total = r.total;
        });
    };

    $scope.search();


});
