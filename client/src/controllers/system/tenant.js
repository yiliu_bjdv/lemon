/**
 * @fileoverview 租户操作
 * @author wing ying_gong@bjdv.com
 */
'use strict';
var app = angular.module('app');
app.controller('TenantCtrl', function ($scope, $state, $notify, $uibModal, $tenant) {
    $scope.page = { limit: 10 };
    $scope.search = function (page) {
        page = page || $scope.page.page;
        $tenant.findAllPage({$or: [{alias:{$like: '%'+$scope.info+'%'}}, {name:{$like: '%'+$scope.info+'%'}}]}, page, $scope.page.limit, ['id']).then(function (r) {
            $scope.tenants = r.rows;
            $scope.page.page = r.page;
            $scope.page.limit = r.limit;
            $scope.page.count = r.count;
            $scope.page.total = r.total;
        });
    };

    $scope.search();

    $scope.add = function () {
        $uibModal.open({
            size: 'lg',
            controller: 'TenantAddCtrl',
            templateUrl: 'views/system/tenant_add.html',
        }).result.then(function () {
            $scope.search();
        }, function () {
            $scope.search();
        });
    };

    $scope.edit = function (tenant) {
        $uibModal.open({
            size: 'lg',
            controller: 'TenantEditCtrl',
            templateUrl: 'views/system/tenant_edit.html',
            resolve: {
                tenant: function () {
                    return tenant;
                }
            }
        }).result.then(function () {
            $scope.search();
        }, function () {
            $scope.search();
        });
    };

    $scope.remove = function (tenant) {
        $tenant.remove(tenant.id).then(function () {
            $notify.info('租户', '删除成功');
            $scope.search();
        }).catch(function (e) {
            $notify.warning('租户', e.message);
            $scope.search();
        });
    };
});

app.controller('TenantAddCtrl', function ($scope, $state, $notify, $uibModalInstance, $tenant, $permission,$queue) {
    $scope.tenant = {status:1,create_at:new Date()};
    
    $scope.save = function () {
        $tenant.create($scope.tenant).then(function () {
            $notify.info('修改租户', '成功');
            $uibModalInstance.close();
        }).catch(function (e) {
            $notify.warning('修改租户', e.message);
            $uibModalInstance.close();
        });
    };

    $scope.cancel = function () {
        $uibModalInstance.dismiss();
    };
});

app.controller('TenantEditCtrl', function ($scope, $state, $notify, $uibModalInstance, tenant, $tenant, $permission,$queue) {
    $scope.tenant = tenant;
    $permission.findAll({},[], ['id', 'name']).then(function(rows){
        $scope.permissions = rows;
    });
    $queue.findAll({'status':1},[], ['id', 'name']).then(function(rows){
        $scope.queues = rows;
    });

    $scope.save = function () { 
        $tenant.save($scope.tenant.id, $scope.tenant).then(function () {
            $notify.info('修改租户', '成功');
            $uibModalInstance.close();
        }).catch(function (e) {
            $notify.warning('修改租户', e.message);
            $uibModalInstance.close();
        });
    };

    $scope.cancel = function () {
        $uibModalInstance.dismiss();
    };
});