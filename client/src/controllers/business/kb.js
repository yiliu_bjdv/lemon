/**
 * @fileOverview 知识库操作
 * @author wing ying_gong@bjdv.com
 */
'use strict';
var app = angular.module('app');

app.controller('KbDirectoryCtrl', function ($scope, $notify, $uibModal, $kb_directory) {
    $scope.data = {};

    var recursive = function (node, id, nodes) {
        var row = node;
        if (row.id == id) {
            row.nodes = nodes;
        } else {
            _.each(row.nodes || [], function (el) {
                recursive(el, id, nodes);
            });
        }
    };

    $scope.collapse = function (node) {
        node.nodes = [];
    };

    $scope.expand = function (id) {
        id = id || null;
        $kb_directory.son(id).then(function (rows) {
            _.each(rows, function (d) {
                d.spaces = "｜－－";
                _.times(d.depth, function () {
                    d.spaces += "｜－－";
                });
            });
            if (!id) {
                $scope.data.nodes = rows;
            } else {
                recursive($scope.data, id, rows);
            }
            if (rows.length == 0) {
                $notify.info('没有下级');
            }
        });
    };

    $scope.expand();

    $scope.init = function () {
        $uibModal.open({
            size: 'sm',
            templateUrl: 'kb_directory_add.html',
            controller: 'KbDirectoryAddCtrl',
            resolve: {
                node: function () {
                    return {};
                }
            }
        }).result.then(function () {
            $scope.expand();
        }, function () {
        });
    };

    $scope.add = function (node) {
        $uibModal.open({
            size: 'sm',
            templateUrl: 'kb_directory_add.html',
            controller: 'KbDirectoryAddCtrl',
            resolve: {
                node: function () {
                    return node;
                }
            }
        }).result.then(function () {
            $scope.expand(node.id);
        }, function () {
        });
    };

    $scope.edit = function (node) {
        $uibModal.open({
            size: 'sm',
            templateUrl: 'kb_directory_edit.html',
            controller: 'KbDirectoryEditCtrl',
            resolve: {
                node: function () {
                    return node;
                }
            }
        }).result.then(function () {
            $scope.expand(node.parent);
        }, function () {
        });
    };

    $scope.file = function (node) {
        $uibModal.open({
            size: 'md',
            templateUrl: 'kb_file_add.html',
            controller: 'KbFileAddCtrl',
            resolve: {
                node: function () {
                    return node;
                }
            }
        }).result.then(function () {
        }, function () {
        });
    };

    $scope.view = function (node) {
        $uibModal.open({
            size: 'md',
            templateUrl: 'kb_file.html',
            controller: 'KbFileCtrl',
            resolve: {
                node: function () {
                    return node;
                }
            }
        }).result.then(function () {
        }, function () {
        });
    };


    $scope.remove = function (node) {
        return $kb_directory.remove(node.id).then(function () {
            $notify.success('删除节点', '成功');
            $scope.expand(node.parent);
        }).catch(function (e) {
            $notify.warning('删除节点', e.message);
        });
    };

});

app.controller('KbDirectoryAddCtrl', function ($scope, $notify, $uibModalInstance, $kb_directory, node) {
    $scope.kb_directory = {status:1};
    $scope.save = function () {
        var parent = node ? node.id : undefined;
        $kb_directory.create($scope.kb_directory, parent).then(function () {
            $notify.success('添加节点', "成功");
            $uibModalInstance.close();
        }).catch(function (e) {
            $notify.warning('添加节点', e.message);
            $uibModalInstance.close();
        });
    };
    $scope.cancel = function () {
        $uibModalInstance.dismiss();
    };
});

app.controller('KbDirectoryEditCtrl', function ($scope, $notify, $uibModalInstance, $kb_directory, node) {
    $scope.kb_directory = node;
    $scope.save = function () {
        $kb_directory.save($scope.kb_directory.id, $scope.kb_directory).then(function () {
            $notify.success('修改节点', "成功");
            $uibModalInstance.close();
        }).catch(function (e) {
            $notify.warning('修改节点', e.message);
            $uibModalInstance.close();
        });
    };
    $scope.cancel = function () {
        $uibModalInstance.dismiss();
    };
});

app.controller('KbFileCtrl', function ($scope, $notify, $timeout, $uibModalInstance,$kb_file, $agent, node,$uibModal) {
    var parent = node ? node.id : undefined;

    $scope.page = { limit: 10 };
    $scope.search = function (page) {
        page = page || $scope.page.page;
        var where;
        if($scope.info==undefined || $scope.info==''){
            where = {parent:parent};
        }else{
            where = {parent:parent,
                     $or: [{name:{$like: '%'+$scope.info+'%'}}, {content:{$like: '%'+$scope.info+'%'}}]
                    };
        }   

        $kb_file.findAllPage(where, page, $scope.page.limit, ['id']).then(function (r) {
            $scope.files = r.rows;
            $scope.page.page = r.page;
            $scope.page.limit = r.limit;
            $scope.page.count = r.count;
            $scope.page.total = r.total;
        });
    };

    $scope.search();

    $scope.add = function () {
        $uibModal.open({
            size: 'md',
            templateUrl: 'kb_file_add.html',
            controller: 'KbFileAddCtrl',
            resolve: {
                node: function () {
                    return node;
                }
            }
        }).result.then(function () {
            $scope.expand(node.id);
        }, function () {
        });
    };

    $scope.cancel = function () {
        $uibModalInstance.dismiss();
    };
});

app.controller('KbFileAddCtrl', function ($scope, $notify, $timeout, $uibModalInstance, $kb_file, $agent, node) {
    var parent = node ? node.id : undefined;
    $scope.kb_file = {parent:parent,status:1};
    $scope.save = function () {
        $kb_file.create($scope.kb_file).then(function () {
            $notify.success('添加节点', "成功");
            $uibModalInstance.close();
        }).catch(function (e) {
            $notify.warning('添加节点', e.message);
            $uibModalInstance.close();
        });
    };
    $scope.cancel = function () {
        $uibModalInstance.dismiss();
    };

    $scope.cancel = function () {
        $uibModalInstance.dismiss();
    };
});

app.controller('LuceneCtrl', function ($scope, $notify, $timeout,$kb_file, $uibModal) {

    $scope.page = { limit: 10 };
    $scope.search = function (page) {
        page = page || $scope.page.page;
        var where;
        if($scope.info==undefined || $scope.info==''){
            where = {};
        }else{
            where = {$or: [{name:{$like: '%'+$scope.info+'%'}}, {content:{$like: '%'+$scope.info+'%'}}]};
        }   

        $kb_file.findAllPage(where, page, $scope.page.limit, ['id']).then(function (r) {
            $scope.files = r.rows;
            $scope.page.page = r.page;
            $scope.page.limit = r.limit;
            $scope.page.count = r.count;
            $scope.page.total = r.total;
        });
    };

    $scope.view = function (node) {
        $uibModal.open({
            size: 'md',
            templateUrl: 'views/business/lucene_view.html',
            controller: 'LuceneViewCtrl',
            resolve: {
                node: function () {
                    return node;
                }
            }
        }).result.then(function () {
        }, function () {
        });
    };

    $scope.search();
    
});

app.controller('LuceneViewCtrl', function ($scope, $notify, $timeout, $uibModalInstance,$kb_file, node) {
    
    $scope.file = node;
    $scope.cancel = function () {
        $uibModalInstance.dismiss();
    };
});