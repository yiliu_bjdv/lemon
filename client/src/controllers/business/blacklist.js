/**
 * @fileoverview 坐席操作
 * @author wing ying_gong@bjdv.com
 */
'use strict';
var app = angular.module('app');
app.controller('BlacklistCtrl', function ($scope, $state, $notify, $uibModal, $blacklist) {
    $scope.page = { limit: 10 };
    $scope.search = function (page) {
        page = page || $scope.page.page;
        var where;
        if($scope.info==undefined || $scope.info==''){
            where = {};
        }else{
            where ={$or: [{tel:{$like: '%'+$scope.info+'%'}}, {name:{$like: '%'+$scope.info+'%'}}]};
        }   
        $blacklist.findAllPage(where, page, $scope.page.limit, ['id']).then(function (r) {
            $scope.blacklists = r.rows;
            $scope.page.page = r.page;
            $scope.page.limit = r.limit;
            $scope.page.count = r.count;
            $scope.page.total = r.total;
        });
    };

    $scope.search();

    $scope.add = function () {
        $uibModal.open({
            size: 'lg',
            controller: 'BlacklistAddCtrl',
            templateUrl: 'views/business/blacklist_add.html',
        }).result.then(function () {
            $scope.search();
        }, function () {
            $scope.search();
        });
    };

    $scope.edit = function (blacklist) {
        $uibModal.open({
            size: 'lg',
            controller: 'BlacklistEditCtrl',
            templateUrl: 'views/business/blacklist_edit.html',
            resolve: {
                blacklist: function () {
                    return blacklist;
                }
            }
        }).result.then(function () {
            $scope.search();
        }, function () {
            $scope.search();
        });
    };

    $scope.remove = function (blacklist) {
        $blacklist.remove(blacklist.id).then(function () {
            $notify.info('黑名单', '删除成功');
            $scope.search();
        }).catch(function (e) {
            $notify.warning('黑名单', e.message);
            $scope.search();
        });
    };
});

app.controller('BlacklistAddCtrl', function ($scope, $state, $notify, $uibModalInstance, $blacklist, $permission,$metadata) {
    $scope.blacklist = {status:1};

    $metadata.findAll({ status: 1, key: 'blacklist:type' }).then(function (rows) {
        $scope.types = rows;
        return $metadata.findAll({ status: 1, key: 'blacklist:category' });
    }).then(function (rows) {
        $scope.categories = rows;
    });

    $scope.save = function () {
        $scope.blacklist.start_at = moment().format('YYYY-MM-DD'),
        $scope.blacklist.end_at = moment().add( $scope.blacklist.time_limit, 'days').format('YYYY-MM-DD');
        $blacklist.create($scope.blacklist).then(function () {
            $notify.info('添加黑名单', '成功');
            $uibModalInstance.close();
        }).catch(function (e) {
            $notify.warning('添加黑名单', e.message);
            $uibModalInstance.close();
        });
    };

    $scope.cancel = function () {
        $uibModalInstance.dismiss();
    };
});

app.controller('BlacklistEditCtrl', function ($scope, $state, $notify, $uibModalInstance, blacklist, $blacklist, $permission,$metadata) {
    $scope.blacklist = blacklist;

    $metadata.findAll({ status: 1, key: 'blacklist:type' }).then(function (rows) {
        $scope.types = rows;
        return $metadata.findAll({ status: 1, key: 'blacklist:category' });
    }).then(function (rows) {
        $scope.categories = rows;
    });

    $scope.save = function () { 
        $blacklist.save($scope.blacklist.id, $scope.blacklist).then(function () {
            $notify.info('修改黑名单', '成功');
            $uibModalInstance.close();
        }).catch(function (e) {
            $notify.warning('修改黑名单', e.message);
            $uibModalInstance.close();
        });
    };

    $scope.cancel = function () {
        $uibModalInstance.dismiss();
    };
});