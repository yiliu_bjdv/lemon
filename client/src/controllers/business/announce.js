/**
 * @fileoverview 公告操作
 * @author wing ying_gong@bjdv.com
 */
'use strict';
var app = angular.module('app');
app.controller('AnnounceCtrl', function ($scope, $state, $notify, $uibModal, $announce) {
    $scope.page = { limit: 10 };
    $scope.search = function (page) {
        page = page || $scope.page.page;
        var where;
        if($scope.info==undefined || $scope.info==''){
            where = {};
        }else{
            where = {$or: [{title:{$like: '%'+$scope.info+'%'}}, {content:{$like: '%'+$scope.info+'%'}}]};
        }   

        $announce.findAllPage(where, page, $scope.page.limit, ['id']).then(function (r) {
            $scope.announces = r.rows;
            $scope.page.page = r.page;
            $scope.page.limit = r.limit;
            $scope.page.count = r.count;
            $scope.page.total = r.total;
        });
    };

    $scope.search();

    $scope.add = function () {
        $uibModal.open({
            size: 'lg',
            controller: 'AnnounceAddCtrl',
            templateUrl: 'views/business/announce_add.html',
        }).result.then(function () {
            $scope.search();
        }, function () {
            $scope.search();
        });
    };

    $scope.edit = function (announce) {
        $uibModal.open({
            size: 'lg',
            controller: 'AnnounceEditCtrl',
            templateUrl: 'views/business/announce_edit.html',
            resolve: {
                announce: function () {
                    return announce;
                }
            }
        }).result.then(function () {
            $scope.search();
        }, function () {
            $scope.search();
        });
    };

    $scope.remove = function (announce) {
        $announce.remove(announce.id).then(function () {
            $notify.info('公告', '删除成功');
            $scope.search();
        }).catch(function (e) {
            $notify.warning('公告', e.message);
            $scope.search();
        });
    };
});

app.controller('AnnounceAddCtrl', function ($scope, $state, $notify, $uibModalInstance, $announce, $permission,$queue) {
    $scope.announce = {status:1};
    $permission.findAll({},[], ['id', 'name']).then(function(rows){
        $scope.permissions = rows;
    });
    $queue.findAll({'status':1},[], ['id', 'name']).then(function(rows){
        $scope.queues = rows;
    });
    $scope.save = function () {
        $announce.create($scope.announce).then(function () {
            $notify.info('添加公告', '成功');
            $uibModalInstance.close();
        }).catch(function (e) {
            $notify.warning('添加公告', e.message);
            $uibModalInstance.close();
        });
    };

    $scope.cancel = function () {
        $uibModalInstance.dismiss();
    };
});

app.controller('AnnounceEditCtrl', function ($scope, $state, $notify, $uibModalInstance, announce, $announce, $permission,$queue) {
    $scope.announce = announce;
    $permission.findAll({},[], ['id', 'name']).then(function(rows){
        $scope.permissions = rows;
    });
    $queue.findAll({'status':1},[], ['id', 'name']).then(function(rows){
        $scope.queues = rows;
    });

    $scope.save = function () { 
        $announce.save($scope.announce.id, $scope.announce).then(function () {
            $notify.info('修改公告', '成功');
            $uibModalInstance.close();
        }).catch(function (e) {
            $notify.warning('修改公告', e.message);
            $uibModalInstance.close();
        });
    };

    $scope.cancel = function () {
        $uibModalInstance.dismiss();
    };
});