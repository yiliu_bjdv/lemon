/**
 * @fileOverview 工单操作
 * @author wing ying_gong@bjdv.com
 */
'use strict';

var app = angular.module('app');
app.controller('ServiceAxbCtrl', function ($scope, $state, $notify, $service_axb, $metadata) {
    $scope.page = { limit: 10 };
    $scope.FlagSearch = false;
    $scope.search = function (page) {
        $scope.page.page = page || 0;
        var where;
        if ($scope.info == undefined || $scope.info == '') {
            where = {};
        } else {
            where = {content: '%'+$scope.info+'%'};
        }
        $service_axb.findAllPage(where,$scope.page.page, $scope.page.limit, ['id'], ['id', 'address', 'customer_id']).then(function (r) {
                $scope.services = r.rows;
                $scope.page.page = r.page;
                $scope.page.limit = r.limit;
                $scope.page.count = r.count;
                $scope.page.total = r.total;
        });
    };

    $scope.search(1);

    /** 跳转修改工单 */
    $scope.edit = function (service) {
        $state.go('app.service.service_axb_edit', { id: service.id });
    };
    /** 跳转修改工单 */
    $scope.add = function () {
        $state.go('app.service.service_axb_add');
    };
});

app.controller('ServiceAxbAddCtrl', function ($scope, $state, $notify,
    $service_axb,
    $stateParams,
    $user,
    UserService,
    $global_meta,
    $uibModal
) {//初始化保费参数
    $scope.picc_fee = 0;
    $scope.suning_commission = 0;
    $scope.fubang_profit = 0;
    $scope.wholesale_price = 0;

    $global_meta.findAll({status: 1}, [], ['context', 'value','key','source','ref_key']).then(function(r){
        $scope.metadatas = r;
    });  

    $scope.axbCard =[];
    $scope.service_axb = {department_id: UserService.user.department_id};
    $scope.service_axb.pic_filename = [];

    $scope.pricechange = function () {
        if($scope.product.category!=""&&$scope.product.category!=undefined&&
            $scope.product.sub_category!=""&&$scope.product.sub_category!=undefined&&
            $scope.service_axb.life!=""&&$scope.service_axb.life!=undefined) {
            $myhttp.get('/axb/service_axb_config', {
                where: {
                    category_id: $scope.product.category || undefined,//品类
                    sub_category_id: $scope.product.sub_category || undefined,//分段类目
                    time_fixed: $scope.service_axb.life || undefined//延保时限
                },
                page: 1,
                limit: 10
            }).then(function (data) {
                console.log(data);
                if (data.count != 0) {
                    $scope.service_axb_config_list = data.data[0];
                    $scope.service_axb.price = $scope.service_axb_config_list.fee;
                    //初始化保费参数
                    $scope.picc_fee = $scope.service_axb_config_list.picc_fee;
                    $scope.suning_commission = $scope.service_axb_config_list.suning_commission;
                    $scope.fubang_profit = $scope.service_axb_config_list.fubang_profit;
                    $scope.wholesale_price = $scope.service_axb_config_list.wholesale_price;;
                } else {
                    $scope.FeeShow = false;
                }
            })
        }
    };

    $scope.add = function () {
        $uibModal.open({
            size: 'lg',
            templateUrl: 'views/service/service_axb_confirm_add.html',
            controller: 'ServiceAxbRealAddCtrl',
            resolve: {
                service_axb: function () {
                    return $scope.service_axb;
                }
            }
        }).result.then(function () {
        }, function () {
        });
    };
});

app.controller('ServiceAxbRealAddCtrl', function ($scope, $state, $notify,
    $service_axb,
    $stateParams,
    $customer,
    $service_category,
    $config,
    $user,
    service_axb,
    $uibModalInstance,
    $axb_card,
    $product
) {

    $scope.cancel = function () {
        $uibModalInstance.dismiss();
    };

    $scope.realAdd = function () {
        $axb_card.findAllPage( {}, 1,10, ['id']).then(function (result) {
                $scope.axbCard = result.rows;
                if ($scope.axbCard && $scope.axbCard.length > 0) {
                    if($scope.axbCard[0].state == 0){
                        $scope.axbCard[0].state = 1;
                        $scope.axbCard[0].registe_at = new Date();

                        //注册数据
                        $scope.axbCard[0].province = $scope.customer.province;
                        $scope.axbCard[0].city = $scope.customer.city;
                        $scope.axbCard[0].area = $scope.customer.area;
                        $scope.axbCard[0].type = $scope.service_axb.type;
                        $scope.axbCard[0].brand = $scope.product.brand;
                        $scope.axbCard[0].model = $scope.product.model;
                        $scope.axbCard[0].category = $scope.product.category;
                        $scope.axbCard[0].sub_category = $scope.product.sub_category;
                        $scope.axbCard[0].life = $scope.service_axb.life;
                        $scope.axbCard[0].service_id = $scope.service_axb.service_id;
                        $scope.axbCard[0].product_price = $scope.service_axb.product_price;
                        $scope.axbCard[0].ori_begin_at = $scope.service_axb.ori_begin_at;
                        $scope.axbCard[0].pic_filename = $scope.service_axb.pic_filename;
                        $scope.axbCard[0].tel = $scope.customer.mobile;
                        $scope.axbCard[0].price = $scope.service_axb.price;

                        //收款数据
                        /* $scope.axbCard[0].receive_state = 1;//收款状态（0：未收款，1已收款）*/
                        $scope.axbCard[0].picc_fee = $scope.picc_fee;
                        $scope.axbCard[0].suning_commission = $scope.suning_commission;
                        $scope.axbCard[0].fubang_profit = $scope.fubang_profit;
                        $scope.axbCard[0].wholesale_price = $scope.wholesale_price;

                        console.log($scope.axbCard[0]);

                        $scope.service_axb.department_id = $scope.axbCard[0].department_id;
                        //铺货数据（需求新增录入字段）
                        service_axb.plain_code = $scope.axbCard[0].plain_code;//明码
                        service_axb.terminal_code = $scope.axbCard[0].terminal_code;//网点代码（BP号）
                        service_axb.terminal_city = $scope.axbCard[0].terminal_city;//网点城市
                        service_axb.terminal_parent = $scope.axbCard[0].terminal_parent;//网点上级
                        service_axb.terminal_description = $scope.axbCard[0].terminal_description;//网点描述
                        service_axb.Express_at = $scope.axbCard[0].Express_at;//延保卡寄送时间
                        service_axb.Express_Number = $scope.axbCard[0].Express_Number;//延保卡寄送快递单号

                        $service_axb.save($scope.axbCard[0].id, $scope.axbCard[0]).then(function () {
                        }, function (e) {
                            CommonService.error(e);
                        }).then(function(){
                            $service_axb.create(service_axb).then(function () {
                                $notify.info('新工单', '保存成功');
                            }).catch(function (e) {
                                $notify.warning('新工单', e.message);
                            });
                        })
                    }else{
                        $notify.warning('新工单', e.message);
                        $scope.cancel();
                    }

                } else {
                    $notify.warning('没有查到相关卡信息，新工单添加失败');
                    $scope.cancel();
                }
            }
        )
    };

    $scope.searchProduct = function () {
        $product.findAllPage( { model: $scope.product.model },1,1 ).then(function (data) {
            if (data.data.length > 0) {
                $scope.product = data.data[0];
            } else {
                $notify.warning('无此型号商品,不能补全');
            }
        })
    };

    $scope.searchCustomer = function () {
        $customer.findAllPage(
            {name: $scope.customer.name, mobile: $scope.customer.mobile},1, 1 ).then(function (data) {
            if (data.data.length > 0) {
                $scope.customer = data.data[0];
            } else {
                $notify.warning('无此手机姓名顾客,不能补全');
            }
        })
    }

});

app.controller('ServiceAxbEditCtrl', function ($scope, $state, $notify,
    $service_axb,
    $stateParams,
    $customer,
    $service_category,
    $config,
    $user,
    ProgressService
) {
    $scope.service_axb = {};

    $user.findAll({ status: 1 }, [['code']], ['id', 'name', 'department_id']).then(function (rows) {
        $scope.agents = rows;
    });

    $service_category.findAll({ status: 1 }).then(function (rows) {
        $scope.categories = rows;
    });

    $service_axb.findOne($stateParams.id).then(function (data) {
        $scope.service_axb = data;
    }).then(function () {
        if ($scope.service_axb.customer_id) {
            $customer.findOne($scope.service_axb.customer_id).then(function (customer) {
                $scope.service_axb.customer = customer;
            });
        }
        $scope.searchProgresses();
    });

    /** 进度 */
    $scope.searchProgresses = function () {
        ProgressService.searchProgressesByCategory($scope.service_axb.service_type).then(function (rows) {
            $scope.progresses = rows;
            $scope.progressChange();
        });
    };

    $scope.progressChange = function () {
        var curProgress = _.find($scope.progresses, { id: $scope.service_axb.progress_id });
        curProgress = curProgress || { seq: 0 };
        _.each($scope.progresses, function (progress) {
            if (progress.seq > curProgress.seq) {
                progress.hide = true;
            } else {
                progress.hide = false;
            }
            _.each(progress.fields, function (field) {
                if (field.field_type == 'linkage' && $scope.service_axb.extra['f' + field.id].v1) {
                    $scope.linkageChange(field, $scope.service_axb.extra['f' + field.id].v1);
                }
            });
        });

    };

    $scope.searchCategories = function () {
        ProgressService.searchCategories().then(function (rows) {
            $scope.categories = rows;
        });
    };

    $scope.searchCategories();

    $scope.linkageChange = function (field, selected) {
        ProgressService.linkageMetadata2(field.id, selected).then(function (rows) {
            field.metadata2 = rows;
        });
    };

    $scope.save = function (state) {
        $scope.service_axb.state_id = state;
        $service.save($scope.service_axb.id, $scope.service_axb).then(function () {
            $notify.info('工单' + $scope.service_axb.id, '保存成功');
        }).catch(function (e) {
            $notify.warning('工单' + $scope.service_axb.id, e.message);
        });
    };

});