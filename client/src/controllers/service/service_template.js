/**
 * @fileoverview 工单模板配置
 * @author wing ying_gong@bjdv.com
 */
'use strict';
var app = angular.module('app');
app.controller('ServiceTemplateCtrl', function ($scope, $state, $uibModal, $notify,
    ProgressService,
    $service_progress,
    $service_progress_field,
    $service_category,
    $service_category_progress,
    $metadata,
    $global_meta
) {

    /** 进度 */
    $scope.searchProgresses = function () {
        ProgressService.searchProgresses().then(function(rows){
            $scope.progresses = rows;
        });
    };

    $scope.searchFields = function (progress) {
        ProgressService.searchFields(progress.id).then(function(rows){
            progress.fields = rows;
        });
    };

    $scope.searchProgresses();

    $scope.addProgress = function () {
        $uibModal.open({
            size: 'sm',
            templateUrl: 'service_progress_add.html',
            controller: 'ServiceProgressAddCtrl'
        }).result.then(function () {
            $scope.searchProgresses();
        }, function () {
            $scope.searchProgresses();
        });
    };

    $scope.editProgress = function () {
        $uibModal.open({
            size: 'sm',
            templateUrl: 'service_progress_edit.html',
            controller: 'ServiceProgressEditCtrl',
            resolve: {
                progress_id: function () {
                    return $scope.selectedProgressId;
                }
            }
        }).result.then(function () {
            $scope.searchProgresses();
        }, function () {
            $scope.searchProgresses();
        });
    };

    $scope.removeProgress = function () {
        $service_progress.remove($scope.selectedProgressId).then(function () {
            $notify.info('工单进度', '删除成功');
            $scope.searchProgresses();
        }).catch(function (e) {
            $notify.warning('工单进度', e.message);
            $scope.searchProgresses();
        });
    };

    /** 分类 */
    $scope.searchCategories = function () {
        ProgressService.searchCategories().then(function (rows) {
            $scope.categories = rows;
        });
    };

    $scope.searchCategories();

    $scope.addCategory = function () {
        $uibModal.open({
            size: 'sm',
            templateUrl: 'service_category_add.html',
            controller: 'ServiceCategoryAddCtrl'
        }).result.then(function () {
            $scope.searchCategories();
        }, function () {
            $scope.searchCategories();
        });
    };

    $scope.editCategory = function () {
        $uibModal.open({
            size: 'sm',
            templateUrl: 'service_category_edit.html',
            controller: 'ServiceCategoryEditCtrl',
            resolve: {
                category_id: function () {
                    return $scope.selectedCategoryId;
                }
            }
        }).result.then(function () {
            $scope.searchCategories();
        }, function () {
            $scope.searchCategories();
        });
    };

    $scope.removeCategory = function () {
        $service_category.remove($scope.selectedCategoryId).then(function () {
            return $service_category_progress.update($scope.selectedCategoryId, []);
        }).then(function () {
            $notify.info('工单分类', '删除成功');
            $scope.searchCategories();
        }).catch(function (e) {
            $notify.warning('工单分类', e.message);
            $scope.searchCategories();
        });
    };

    /** 表单内容 */
    $global_meta.findAll({ status: 1, key: 'service:field' }).then(function (rows) {
        $scope.fieldTypes = rows;
    });


    $scope.changeMetadata = function (key, metadata) {
        $uibModal.open({
            size: 'md',
            templateUrl: 'field_metadata.html',
            controller: 'FieldMetadataCtrl',
            resolve: {
                key: function () {
                    return key;
                }
            }
        }).result.then(function () {
        }, function (rows) {
            _.remove(metadata);
            _.each(rows, function (row) {
                metadata.push(row);
            });
        });
    };
    $scope.linkageChange = function (field, key) {
        $metadata.findAll({ status: 1, key: key }).then(function (rows) {
            field.metadata2 = rows;
        });
    };
    $scope.fieldTypeChange = function (field) {
        if (field.field_type == 'select' || field.field_type == 'tags') {
            $metadata.findAll({ status: 1, key: 'field:' + field.id }).then(function (rows) {
                field.metadata = rows;
            });
        } else {
            $metadata.findAll({ status: 1, key: 'field:' + field.id }).then(function (rows) {
                field.metadata1 = rows;
            });
        }
    };

    $scope.addField = function (progress) {
        progress.field.progress_id = progress.id;
        progress.field.status = 1;
        $service_progress_field.create(progress.field).then(function () {
            $notify.info('进度表单项', '添加成功');
            $scope.searchFields(progress);
        }).catch(function (e) {
            $notify.warning('进度表单项', e.message);
        });
    };

    $scope.saveField = function (progress, field) {
        $service_progress_field.save(field.id, field).then(function () {
            $notify.info('进度表单项', '保存成功');
            $scope.searchFields(progress);
        }).catch(function (e) {
            $notify.warning('进度表单项', e.message);
        });
    };

    $scope.removeField = function (progress, field) {
        $service_progress_field.remove(field.id).then(function () {
            $notify.info('进度表单项', '删除成功');
            $scope.searchFields(progress);
        }).catch(function (e) {
            $notify.warning('进度表单项', e.message);
        });
    };
});

app.controller('ServiceProgressAddCtrl', function ($scope, $notify, $uibModalInstance, $service_progress) {
    $scope.progress = { status: 1 };
    $scope.save = function () {
        $service_progress.create($scope.progress).then(function () {
            $notify.info('工单进度', '保存成功');
            $uibModalInstance.close();
        }).catch(function (e) {
            $notify.warning('工单进度', e.message);
        });
    };
    $scope.cancel = function () {
        $uibModalInstance.dismiss();
    };
});

app.controller('ServiceProgressEditCtrl', function ($scope, $notify, $uibModalInstance, progress_id, $service_progress) {
    $service_progress.findOne(progress_id, ['id', 'name', 'seq', 'status']).then(function (row) {
        $scope.progress = row;
    }).catch(function (e) {
        $notify.warning('工单进度', e.message);
    });
    $scope.save = function () {
        $service_progress.save($scope.progress.id, $scope.progress).then(function () {
            $notify.info('工单进度', '保存成功');
            $uibModalInstance.close();
        }).catch(function (e) {
            $notify.warning('工单进度', e.message);
        });
    };
    $scope.cancel = function () {
        $uibModalInstance.dismiss();
    };
});

app.controller('ServiceCategoryAddCtrl', function ($scope, $notify, $uibModalInstance, $service_category) {
    $scope.category = { status: 1 };
    
    $scope.save = function () {
        $service_category.create($scope.category).then(function () {
            $notify.info('工单分类', '保存成功');
            $uibModalInstance.close();
        }).catch(function (e) {
            $notify.warning('工单分类', e.message);
        });
    };
    $scope.cancel = function () {
        $uibModalInstance.dismiss();
    };
});

app.controller('ServiceCategoryEditCtrl', function ($scope, $notify, $uibModalInstance, category_id, $service_category, $service_progress, $service_category_progress) {
    $service_category.findOne(category_id, ['id', 'name', 'status']).then(function (row) {
        $scope.category = row;
        return $service_progress.findAll({ status: 1 }, [], ['id', 'name']);        
    }).then(function (rows) {
        $scope.progresses = rows;        
        return $service_category_progress.findAll({ category_id: $scope.category.id });
    }).then(function (rows) {
        $scope.pids = _.map(rows, 'progress_id');
    }).catch(function (e) {
        $notify.warning('工单分类', e.message);
    });
    $scope.save = function () {
        $service_category.save($scope.category.id, $scope.category).then(function () {
            return $service_category_progress.update(category_id, $scope.pids);
        }).then(function () {
            $notify.info('工单分类', '保存成功');
            $uibModalInstance.close();
        }).catch(function (e) {
            $notify.warning('工单分类', e.message);
        });
    };
    $scope.cancel = function () {
        $uibModalInstance.dismiss();
    };
});

app.controller('FieldMetadataCtrl', function ($scope, $notify, $uibModalInstance, key, $metadata) {
    $scope.meta = { key: key, status: 1 };
    $scope.search = function () {
        $metadata.findAll({ status: 1, key: key }).then(function (rows) {
            $scope.metas = rows;
        });
    };
    $scope.search();

    $scope.add = function () {
        $metadata.create($scope.meta).then(function () {
            $notify.info('下拉项', '保存成功');
            $scope.meta = { key: key, status: 1 };
            $scope.search();
        }).catch(function (e) {
            $notify.warning('下拉项', e.message);
        });
    };

    $scope.save = function (meta) {
        $metadata.save(meta.id, meta).then(function () {
            $notify.info('下拉项', '保存成功');
            $scope.search();
        }).catch(function (e) {
            $notify.warning('下拉项', e.message);
        });
    };

    $scope.remove = function (meta) {
        $metadata.remove(meta.id).then(function () {
            $notify.info('下拉项', '删除成功');
            $scope.search();
        }).catch(function (e) {
            $notify.warning('下拉项', e.message);
        });
    };

    $scope.cancel = function () {
        $uibModalInstance.dismiss($scope.metas);
    };
});