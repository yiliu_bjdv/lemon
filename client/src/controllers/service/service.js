/**
 * @fileOverview 工单操作
 * @author wing ying_gong@bjdv.com
 */
'use strict';

var app = angular.module('app');
app.controller('ServiceCtrl', function ($scope, $state, $notify, $service, $metadata) {
    $scope.page = { limit: 10 };

    $scope.search = function (page) {
        $scope.page.page = page || 0;
        var where;
        if ($scope.info == undefined || $scope.info == '') {
            where = {};
        } else {
            where = {address: '%'+$scope.info+'%'};
        }
        $service.findAllPage(where, $scope.page.page, $scope.page.limit, ['id'], ['id', 'service_type', 'address', 'customer_id']).then(function (r) {
               //$scope.services = r;
                $scope.services = r.rows;
                $scope.page.page = r.page;
                $scope.page.limit = r.limit;
                $scope.page.count = r.count;
                $scope.page.total = r.total;
        });
    };

    $scope.search();

    /** 跳转修改工单 */
    $scope.edit = function (service) {
        $state.go('app.service.service_edit', { id: service.id });
    };
});

app.controller('ServiceAddCtrl', function ($scope, $state, $notify,
    $service,
    $stateParams,
    ProgressService,
    $user
) {
    $user.findAll({ status: 1 }, [['code']], ['id', 'name', 'department_id']).then(function (rows) {
        $scope.agents = rows;
    });
    $scope.service = { customer: {}, extra: {}, service_type: $stateParams.type };
    if($stateParams.type == 13){
        $scope.service.progress_id = 7;
    }
    /** 进度 */
    $scope.searchProgresses = function () {
        ProgressService.searchProgressesByCategory($stateParams.type).then(function (rows) {
            $scope.progresses = rows;
            $scope.progressChange();
        });
    };

    $scope.progressChange = function () {
        var curProgress = _.find($scope.progresses, { id: $scope.service.progress_id });
        curProgress = curProgress || { seq: 0 };
        _.each($scope.progresses, function (progress) {
            if (progress.seq > curProgress.seq) {
                progress.hide = true;
            } else {
                progress.hide = false;
            }
        });
    };

    $scope.searchFields = function (progress) {
        ProgressService.searchFields(progress.id).then(function (rows) {
            progress.fields = rows;
        });
    };

    $scope.searchProgresses();

    $scope.searchCategories = function () {
        ProgressService.searchCategories().then(function (rows) {
            $scope.categories = rows;
        });
    };

    $scope.searchCategories();

    $scope.linkageChange = function (field, selected) {
        ProgressService.linkageMetadata2(field.id, selected).then(function (rows) {
            field.metadata2 = rows;
        });
    };

    $scope.save = function (state) {
        $scope.service.state_id = state;
        $service.create($scope.service).then(function () {
            $notify.info('新工单', '保存成功');
        }).catch(function (e) {
            $notify.warning('新工单', e.message);
        });
    };
});

app.controller('ServiceEditCtrl', function ($scope, $state, $notify,
    $service,
    $stateParams,
    $customer,
    $service_category,
    $config,
    $user,
    ProgressService
) {
    $scope.service = {};

    $user.findAll({ status: 1 }, [['code']], ['id', 'name', 'department_id']).then(function (rows) {
        $scope.agents = rows;
    });

    $service_category.findAll({ status: 1 }).then(function (rows) {
        $scope.categories = rows;
    });

    $service.findOne($stateParams.id).then(function (data) {
        $scope.service = data;
    }).then(function () {
        if ($scope.service.customer_id) {
            $customer.findOne($scope.service.customer_id).then(function (customer) {
                $scope.service.customer = customer;
            });
        }
        $scope.searchProgresses();
    });

    /** 进度 */
    $scope.searchProgresses = function () {
        ProgressService.searchProgressesByCategory($scope.service.service_type).then(function (rows) {
            $scope.progresses = rows;
            $scope.progressChange();
        });
    };

    $scope.progressChange = function () {
        var curProgress = _.find($scope.progresses, { id: $scope.service.progress_id });
        curProgress = curProgress || { seq: 0 };
        _.each($scope.progresses, function (progress) {
            if (progress.seq > curProgress.seq) {
                progress.hide = true;
            } else {
                progress.hide = false;
            }
            _.each(progress.fields, function (field) {
                if (field.field_type == 'linkage' && $scope.service.extra['f' + field.id].v1) {
                    $scope.linkageChange(field, $scope.service.extra['f' + field.id].v1);
                }
            });
        });

    };

    $scope.searchCategories = function () {
        ProgressService.searchCategories().then(function (rows) {
            $scope.categories = rows;
        });
    };

    $scope.searchCategories();

    $scope.linkageChange = function (field, selected) {
        ProgressService.linkageMetadata2(field.id, selected).then(function (rows) {
            field.metadata2 = rows;
        });
    };

    $scope.save = function (state) {
        $scope.service.state_id = state;
        $service.save($scope.service.id, $scope.service).then(function () {
            $notify.info('工单' + $scope.service.id, '保存成功');
        }).catch(function (e) {
            $notify.warning('工单' + $scope.service.id, e.message);
        });
    };

});