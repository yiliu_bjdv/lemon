/**
 * @fileOverview 认证操作
 * @author wing ying_gong@bjdv.com
 */
'use strict';
var app = angular.module('app');
app.controller('SigninCtrl', function ($scope, $myhttp, $state, $notify, $localStorage,$socket) {
    $scope.login = function () {
        if ($scope.user) {
            $scope.user.password = $.md5($scope.user.passwd);
        }
        $myhttp.post('/access/auth', {
            user_name: $scope.user.user_name,
            password: $scope.user.password
        }).then(function (r) {
            $localStorage.token = r.token;
            $localStorage.user = r.user;
            $socket.register($localStorage.token);
        }).then(function () {            
            $state.go('app.dashboard');
        }).catch(function (e) {
            $notify.warning("登录", e.message);
        });
    };
});

app.controller('SignupCtrl', function ($scope, $state, $notify, $myhttp) {
    $scope.signup = function () {
        $scope.user.password = $.md5($scope.user.passwd);
        $myhttp.post('/access/signup', $scope.user).then(function () {
            $notify.info('注册', '注册成功');
            $state.go('access.signin');
        }).catch(function (e) {
            $notify.warning('注册', e.message);
        });
    };
});

app.controller('LogoutCtrl', function ($scope, $state, $myhttp, $localStorage, $socket) {
    $scope.logout = function () {
        $socket.unregister($localStorage.token);
        $socket.ctiUnRegister();
        delete $localStorage.token;
        delete $localStorage.user;
        $state.go('access.signin');
    };
});

app.controller('UserInfoCtrl', function($scope, UserService){
    $scope.user = UserService.user;
});