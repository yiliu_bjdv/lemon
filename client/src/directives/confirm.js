'use strict';
var app = angular.module('app');
app.directive('confirm', function () {
    return {
        restrict: 'AE',
        scope: {
            confirm: '&',
            message: '@'
        },
        link: function (scope, element) {
            $(element).click(function () {
                scope.click();
            });
        },
        controller: function ($scope, $uibModal) {
            $scope.click = function () {
                $uibModal.open({
                    size: 'sm',
                    templateUrl: 'views/blocks/confirm.html',
                    resolve: {
                        message: function(){
                            return $scope.message;
                        }
                    },
                    controller: function ($scope, $uibModalInstance, message) {
                        $scope.tips = {message: message};
                        $scope.ok = function () {
                            $uibModalInstance.close();
                        };
                        $scope.cancel = function () {
                            $uibModalInstance.dismiss('close');
                        };
                    }
                }).result.then(function () {
                    if ($scope.confirm) {
                        $scope.confirm();
                    }
                });
            };
        }
    };
});