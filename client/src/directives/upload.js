'use strict';
var app = angular.module('app');
app.directive('upload', function () {
    return {
        restrict: 'AE',
        require: '^ngModel',
        scope: {
            ngModel: '=',
            upload: '@'
        },
        templateUrl: 'views/blocks/upload.html',
        link: function (scope, elem) {
            scope.browser = function () {
                $(elem).find('[type="file"]').trigger('click');
            };
        },
        controller: function ($scope, $timeout, Upload, $notify) {
            $scope.uploadFile = function () {
                if ($scope.file) {
                    Upload.upload({
                        url: '/api/file/' + $scope.upload,
                        data: { file: $scope.file },
                    }).then(function (response) {
                        $timeout(function () {
                            $scope.ngModel = response.data;
                            $notify.info('文件', response.data);
                        });
                    }, function (response) {
                        if (response.status > 0) {
                            var errorMsg = response.status + ': ' + response.data;
                            $notify.warning('文件', errorMsg);
                        }
                    }, function (evt) {
                        $scope.progress = Math.min(100, parseInt(100.0 * evt.loaded / evt.total));
                    });
                }
            };
        }
    };
});

app.directive('import', function () {
    return {
        restrict: 'AE',
        require: '^ngModel',
        scope: {
            ngModel: '='
        },
        templateUrl: 'views/blocks/upload.html',
        link: function (scope, elem) {
            scope.browser = function () {
                $(elem).find('[type="file"]').trigger('click');
            };
        },
        controller: function ($scope, $timeout, Upload, $notify) {

            $scope.uploadFile = function () {
                Upload.upload({
                    url: '/api/file/import',
                    data: { file: $scope.file },
                }).then(function (response) {
                    $timeout(function () {
                        $scope.ngModel = response.data;
                        $notify.info('文件', response.data);
                    });
                }, function (response) {
                    if (response.status > 0) {
                        var errorMsg = response.status + ': ' + response.data;
                        $notify.warning('文件', errorMsg);
                    }
                }, function (evt) {
                    $scope.progress = Math.min(100, parseInt(100.0 * evt.loaded / evt.total));
                });
            };
        }
    };
});