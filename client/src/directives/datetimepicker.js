'use strict';
var app = angular.module('app');

app.directive('datetimepicker', function(){
    return {
        restrict: 'AE',
        link: function(scope, elm){
            $(elm).datetimepicker({
                "lang": "ch",
                'format' : 'Y-m-d H:i',
                'closeOnDateSelect': true,
                step: 5,
                mask: true
            });
        }
    }
});

app.directive('mytimepicker', function(){
    return {
        restrict: 'AE',
        link: function(scope, elm){
            $(elm).datetimepicker({
                lang: "ch",
                datepicker: false,
                format: 'H:i',
                closeOnDateSelect: true,
                step: 30,
                mask: true
            });
        }
    }
});

app.directive('mydatepicker', function(){
    return {
        restrict: 'AE',
        link: function(scope, elm){
            $(elm).datetimepicker({
                lang: "ch",
                timepicker: false,
                format: 'Y-m-d',
                closeOnDateSelect: true,
                mask: true
            });
        }
    }
});
