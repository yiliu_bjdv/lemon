/**
 * @fileOverview angular指令集
 * @author wing ying_gong@bjdv.com
 */
'use strict';
var app = angular.module('app');

/** 部门 */
app.directive('department', function () {
    return {
        restrict: 'AE',
        scope: {
            department: '@'
        },
        template: "<span>{{data.name}}</span>",
        controller: function ($scope, $department) {
            $scope.$watch('department', function () {
                if ($scope.department) {
                    $department.fetch($scope.department).then(function (data) {
                        $scope.data = data;
                    });
                }
            });
        }
    };
});

/** 名称翻译 */
app.directive('transContext', function (GlobalMetaService) {
    return {
        restrict: 'AE',
        scope: {
            transVal: '@',
            transContext: '@'
        },
        template: "{{name}}",
        controller: function ($scope) {
            if (!_.isUndefined($scope.transVal)) {
                GlobalMetaService.trans($scope.transContext, $scope.transVal).then(function (r) {
                    $scope.name = r;
                }).catch(function () {
                    $scope.name = "";
                });
            }
        }
    };
});

/** 翻译工单类别 */
app.directive('transCategory', function (GlobalMetaService) {
    return {
        restrict: 'AE',
        scope: {
            transCategory: '@'
        },
        template: "{{name}}",
        controller: function ($scope, $service_category) {
            if (!_.isUndefined($scope.transCategory)) {
                $service_category.findOne($scope.transCategory, ['name']).then(function (r) {
                    $scope.name = r.name;
                }).catch(function () {
                    $scope.name = "";
                });
            }
        }
    };
});

/** 翻译客户 */
app.directive('transCustomer', function (GlobalMetaService) {
    return {
        restrict: 'AE',
        scope: {
            transCustomer: '@'
        },
        template: "{{name}}",
        controller: function ($scope, $customer) {
            if (!_.isUndefined($scope.transCustomer)) {
                $customer.findOne($scope.transCustomer, ['name','tel']).then(function (r) {
                    $scope.name = r.name+'('+r.tel+')';
                }).catch(function () {
                    $scope.name = "";
                });
            }
        }
    };
});



/** 状态翻译 */
app.directive('transStatus', function (GlobalMetaService) {
    return {
        restrict: 'AE',
        scope: {
            transStatus: '@'
        },
        template: "{{name}}",
        controller: function ($scope) {
            if (!_.isUndefined($scope.transStatus)) {
                GlobalMetaService.trans('status', $scope.transStatus).then(function (r) {
                    $scope.name = r;
                }).catch(function () {
                    $scope.name = "";
                });
            }
        }
    };
});

/**
 * 队列策略
 */
app.directive('transStrategy', function (GlobalMetaService) {
    return {
        restrict: 'AE',
        scope: {
            transStrategy: '@'
        },
        template: "{{name}}",
        controller: function ($scope) {
            if (!_.isUndefined($scope.transStrategy)) {
                GlobalMetaService.trans('queue:strategy', $scope.transStrategy).then(function (r) {
                    $scope.name = r;
                }).catch(function () {
                    $scope.name = "";
                });
            }
        }
    };
});

/** 
 * 坐席翻译
 */
app.directive('transUser', function () {
    return {
        restrict: 'AE',
        scope: {
            transUser: '@'
        },
        template: "{{name}}",
        controller: function ($scope, $user) {
            if (!_.isUndefined($scope.transUser)) {
                $user.findOne($scope.transUser, ['name']).then(function (r) {
                    $scope.name = r.name;
                }).catch(function () {
                    $scope.name = "";
                });
            }
        }
    };
});

app.directive('transLevel', function () {
    return {
        restrict: 'AE',
        scope: {
            transLevel: '@'
        },
        template: "{{name}}",
        controller: function ($scope, $level) {
            if (!_.isUndefined($scope.transLevel)) {
                $level.findAll({ level: $scope.transLevel }, [], ['name']).then(function (rows) {
                    if (rows.length > 0) {
                        $scope.name = rows[0].name;
                    }
                }).catch(function () {
                    $scope.name = "";
                });
            }
        }
    };
});

app.directive('transType', function () {
    return {
        restrict: 'AE',
        scope: {
            transType: '@'
        },
        template: "{{name}}",
        controller: function ($scope, GlobalMetaService) {
            if (!_.isUndefined($scope.transType)) {
                GlobalMetaService.trans('cti:type', $scope.transType).then(function (r) {
                    $scope.name = r;
                }).catch(function () {
                    $scope.name = "";
                });
            }
        }
    };
});

app.directive('transTarget', function () {
    return {
        restrict: 'AE',
        scope: {
            targetType: '@',
            transTarget: '@'
        },
        template: "{{name}}",
        controller: function ($scope, $agent, $ivr, $queue) {
            if (!_.isUndefined($scope.transTarget && !_.isUndefined($scope.targetType))) {
                Promise.resolve().then(function () {
                    if ($scope.targetType == '3') {
                        return $queue.findOne($scope.transTarget, ['name']);
                    } else if ($scope.targetType == '4') {
                        return $ivr.findOne($scope.transTarget, ['name']);
                    } else if ($scope.targetType == '5') {
                        return $agent.findOne($scope.transTarget, ['name']);
                    }
                }).then(function (row) {
                    if ($scope.targetType == '1') {
                        $scope.name = $scope.transTarget;
                    } else
                        $scope.name = row.name;
                }).catch(function () {
                    $scope.name = "";
                });
            }
        }
    };
});

/**
 * 地区联动
 */
app.directive('areaStreet', function () {
    return {
        restrict: 'AE',
        scope: {
            areaStreet: '='
        },
        templateUrl: 'views/blocks/street.html',
        controller: function ($scope, $area_street) {
            $scope.$watch('::areaStreet', function () {
                if (!$scope.areaStreet || $scope.areaStreet.length < 2) {
                    $scope.province = "";
                }
                if ($scope.areaStreet && $scope.areaStreet.length >= 2) {
                    $scope.province = $scope.areaStreet.substr(0, 2);
                }
                if ($scope.areaStreet && $scope.areaStreet.length >= 4) {
                    $scope.city = $scope.areaStreet.substr(0, 4);
                }
                if ($scope.areaStreet && $scope.areaStreet.length >= 6) {
                    $scope.area = $scope.areaStreet.substr(0, 6);
                }
                if ($scope.areaStreet && $scope.areaStreet.length >= 9) {
                    $scope.street = $scope.areaStreet.substr(0, 9);
                }
                $scope.fill();
            });

            $scope.provinceChange = function () {
                $scope.areaStreet = $scope.province;
                $scope.city = "";
                $scope.cities = [];
                $scope.area = "";
                $scope.areas = [];
                $scope.street = "";
                $scope.streets = [];
                $area_street.findAll($scope.areaStreet).then(function (rows) {
                    $scope.cities = rows;
                });
            };

            $scope.cityChange = function () {
                $scope.areaStreet = $scope.city;
                $scope.area = "";
                $scope.areas = [];
                $scope.street = "";
                $scope.streets = [];
                $area_street.findAll($scope.areaStreet).then(function (rows) {
                    $scope.areas = rows;
                });
            };

            $scope.areaChange = function () {
                $scope.areaStreet = $scope.area;
                $scope.street = "";
                $scope.streets = [];
                $area_street.findAll($scope.areaStreet).then(function (rows) {
                    $scope.streets = rows;
                });
            };

            $scope.streetChange = function () {
                $scope.areaStreet = $scope.street;
            };

            $scope.fill = function () {
                $area_street.findAll("").then(function (rows) {
                    $scope.provinces = rows;
                });
                if ($scope.province) {
                    $area_street.findAll($scope.province).then(function (rows) {
                        $scope.cities = rows;
                    });
                }
                if ($scope.city) {
                    $area_street.findAll($scope.city).then(function (rows) {
                        $scope.areas = rows;
                    });
                }
                if ($scope.area) {
                    $area_street.findAll($scope.area).then(function (rows) {
                        $scope.streets = rows;
                    });
                }
            };
        }
    };
});