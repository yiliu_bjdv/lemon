'use strict';
var app = angular.module('app');
app.directive('image', function () {
    return {
        restrict: 'AE',
        require: '^ngModel',
        scope: {
            ngModel: '='
        },
        templateUrl: 'views/blocks/image.html',
        link: function (scope, elem) {
            scope.browser = function () {
                $(elem).find('[type="file"]').trigger('click');
            };
        },
        controller: function ($scope, Upload, $notify) {
            $scope.convert = function () {
                if ($scope.file && $scope.file.name) {
                    var regx = /\.(JPEG|jpeg|JPG|jpg|GIF|gif|PNG|png)$/;
                    if (regx.test($scope.file.name)) {
                        Upload.base64DataUrl([$scope.file]).then(function (urls) {
                            $scope.ngModel = urls[0];
                        });
                    } else {
                        $notify.warning('图片', '请检查图片格式');
                    }
                }
            };
        }
    };
});