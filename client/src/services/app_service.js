/**
 * @fileOverview app常用服务
 * @author wing ying_gong@bjdv.com
 */
'use strict';
var app = angular.module('app');
var timeout = 15 * 60 * 1000;
app.factory('GlobalMetaService', function ($global_meta) {
    var service = {};
    var metas = {};
    service.trans = function (key, value) {
        var item;
        return Promise.resolve().then(function () {
            item = metas[key];
            var now = new Date().getTime();
            if (!item || now - item.time > timeout) {
                return $global_meta.findAll({ key: key, status: 1 }, [], ['context', 'value']).then(function (r) {
                    metas[key] = { rows: r, time: now };
                    item = metas[key];
                });
            }
        }).then(function () {
            var result = "";
            _.each(item.rows, function (row) {
                if (row.value == value) {
                    result = row.context;
                }
            });
            return result;
        });
    };
    return service;
});

app.factory('MetadataService', function ($metadata, $user, $queue, $ivr) {
    var service = {};
    var metas = {};
    service.trans = function (key, value) {
        var item;
        return Promise.resolve().then(function () {
            item = metas[key];
            var now = new Date().getTime();
            if (!item || now - item.time > timeout) {
                return $metadata.findAll({ key: key, status: 1 }).then(function (r) {
                    metas[key] = { rows: r, time: now };
                    item = metas[key];
                });
            }
        }).then(function () {
            var result = "";
            _.each(item.rows, function (row) {
                if (row.value == value) {
                    result = row.context;
                }
            });
            return result;
        });
    };
    service.transUser = function (id) {
        return $user.findOne({ id: id }, ['name']).then(function (row) {
            return row.name;
        });
    };
    return service;
});
app.factory('ProgressService', function (
    $service_progress,
    $service_category_progress,
    $service_progress_field,
    $service_category,
    $metadata
) {
    var service = {};
    service.searchFields = function (progressId) {
        var fields;
        return $service_progress_field.findAll({ status: 1, progress_id: progressId }, ['seq']).then(function (rows) {
            fields = rows;
            return rows;
        }).each(function (field) {
            if (field.field_type == 'select' || field.field_type == 'tags') {
                return $metadata.findAll({ status: 1, key: 'field:' + field.id }).then(function (rows) {
                    field.metadata = rows;
                });
            } else if (field.field_type == 'linkage') {
                return $metadata.findAll({ status: 1, key: 'field:' + field.id }).then(function (rows) {
                    field.metadata1 = rows;
                });
            }
        }).then(function () {
            return fields;
        });
    };

    service.searchProgresses = function () {
        var progresses;
        return $service_progress.findAll({ status: 1 }, [['seq']]).then(function (rows) {
            progresses = rows;
            return rows;
        }).each(function (progress) {
            return $service_category_progress.findAll({ progress_id: progress.id }).then(function (rows) {
                progress.categories = rows;
                return service.searchFields(progress.id).then(function (rows) {
                    progress.fields = rows;
                });
            });
        }).then(function () {
            return progresses;
        });
    };

    service.searchProgressesByCategory = function (categoryId) {
        var progresses;
        return $service_category_progress.findAll({ category_id: categoryId }).then(function (rows) {
            var cids = _.map(rows, 'progress_id');
            return $service_progress.findAll({ status: 1, id: { $in: cids } }, [['seq']]);
        }).then(function (rows) {
            progresses = rows;
            return rows;
        }).each(function (progress) {
            return service.searchFields(progress.id).then(function (rows) {
                progress.fields = rows;
            });
        }).then(function () {
            return progresses;
        });
    };

    service.searchCategories = function () {
        return $service_category.findAll({ status: 1 });
    };

    service.linkageMetadata2 = function (fieldId, selected) {
        return $metadata.findAll({ status: 1, key: 'field:' + fieldId + ':' + selected }).then(function (rows) {
            return rows;
        });
    };

    return service;
});

app.factory('UserService', function ($myhttp) {
    var service = {};

    service.getUser = function () {
        return $myhttp.get('/api/info').then(function (user) {
            service.user = user;
            return user;
        });
    };

    service.has = function (permission) {
        if (service.user && service.user.permissions) {
            var permissions = service.user.permissions;
            return _.includes(permissions, permission);
        }
        return false;
    };

    return service;
});