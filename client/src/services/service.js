/**
 * @fileOverview 公用服务
 * @author wing ying_gong@bjdv.com
 */
'use strict';
var app = angular.module('app');
app.factory('$myhttp', function ($http, $state) {
    $http.defaults.cache = false;
    var service = {};
    service.get = function (url, params, cache) {
        if (!cache) {
            cache = false;
        }
        if (!params) {
            params = {};
        }
        var promise = new Promise(function (resolve, reject) {
            $http({ method: 'GET', url: url, cache: cache, params: params }).success(function (data) {
                //console.log(data);
                if (data.err) {
                    var err = new Error(data.result);
                    err.res = data.err;
                    reject(err);
                    console.log('get(' + url + ',' + JSON.stringify(params) + ')=' + data.err);
                } else {
                    resolve(data.result);
                }
            }).error(function (data, status) {
                if (status == 401) {
                    $state.go('access.signin');
                    reject(new Error('未授权'));
                } else {
                    reject(new Error('调用异常'));
                    console.log('get(' + url + ',' + JSON.stringify(params) + ')=调用异常');
                }
            });
        });
        return promise;
    };

    service.post = function (url, params, cache) {
        if (!cache) {
            cache = false;
        }
        var promise = new Promise(function (resolve, reject) {
            $http({ method: 'POST', url: url, data: params, cache: cache }).success(function (data) {
                if (data.err) {
                    var err = new Error(data.result);
                    err.res = data.err;
                    reject(err);
                    console.log('post(' + url + ',' + JSON.stringify(params) + ')=' + data.err);
                } else {
                    resolve(data.result);
                }
            }).error(function (data, status) {
                if (status == 401) {
                    $state.go('access.signin');
                    reject(new Error("未授权"));
                } else {
                    reject(new Error('调用异常'));
                    console.log('post(' + url + ',' + JSON.stringify(params) + ')=调用异常');
                }
            });
        });
        return promise;
    };

    service.put = function (url, params, cache) {
        if (!cache) {
            cache = false;
        }
        var promise = new Promise(function (resolve, reject) {
            $http({ method: 'PUT', url: url, data: params, cache: cache }).success(function (data) {
                if (data.err) {
                    var err = new Error(data.result);
                    err.res = data.err;
                    reject(err);
                    console.log('put(' + url + ',' + JSON.stringify(params) + ')=' + data.err);
                } else {
                    resolve(data.result);
                }
            }).error(function (data, status) {
                if (status == 401) {
                    $state.go('access.signin');
                    reject(new Error("未授权"));
                } else {
                    reject(new Error('调用异常'));
                    console.log('put(' + url + ',' + JSON.stringify(params) + ')=调用异常');
                }
            });
        });
        return promise;
    };

    service.del = function (url, cache) {
        if (!cache) {
            cache = false;
        }
        var promise = new Promise(function (resolve, reject) {
            $http({ method: 'DELETE', url: url, cache: cache }).success(function (data) {
                if (data.err) {
                    var err = new Error(data.result);
                    err.res = data.err;
                    reject(err);
                    console.log('delete(' + url + ')=' + data.err);
                } else {
                    resolve(data.result);
                }
            }).error(function (data, status) {
                $state.go('access.signin');
                console.log('delete(' + url + ')=调用异常');
                if (status == 401) {
                    $state.go('access.signin');
                }
            });            
        });
        return promise;
    };

    service.jsonp = function (url, params) {

        return new Promise(function (resolve, reject) {
            if (!params) {
                params = { callback: 'JSON_CALLBACK' };
            } else {
                params.callback = 'JSON_CALLBACK';
            }
            params.timestamp = new Date().getTime();
            params.jsonp = 'jsonp';
            $http({ method: 'jsonp', url: url, cache: false, params: params }).success(function (data) {
                if (data.err) {
                    var err = new Error(data.result);
                    err.res = data.err;
                    reject(err);
                } else {
                    resolve(data.result);
                }
            }).error(function () {
                reject(new Error('调用异常'));
            });
        });
    };
    return service;
});

app.factory('$notify', function ($timeout, toaster) {
    var service = {};
    service.info = function (title, context) {
        toaster.info(title, context);
    };
    service.warning = function (title, context) {
        toaster.warning(title, context);
    };
    service.success = function (title, context) {
        toaster.success(title, context);
    };
    service.error = function (title, context) {
        toaster.error(title, context);
    };
    return service;
});
