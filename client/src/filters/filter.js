/**
 * @fileOverview angular过滤器
 * @author wing ying_gong@bjdv.com
 */
'use strict';
var app = angular.module('app');

app.filter('fromNow', function() {
    return function(date) {
        return moment(date).fromNow();
    };
});

app.filter('date', function(){
    return function(date){
        return moment(date).format('YYYY-MM-DD');
    };
});

app.filter('status', function(){
    return function(status){
        if(status == 0){
            return "停用";
        }else if(status == 1){
            return "正常";
        }else{
            return "未知";
        }
    };
});

app.filter('statusClass', function(){
    return function(status){
        if(status == 0){
            return "danger";
        }else if(status == 1){
            return "success";
        }else{
            return "default";
        }
    };
});