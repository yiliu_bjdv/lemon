'use strict';

/**
* Config for the router
*/
angular.module('app')
.run(['$rootScope', '$state', '$stateParams', '$localStorage', function ($rootScope, $state, $stateParams, $localStorage) {    
    $rootScope.$state = $state;
    $rootScope.$stateParams = $stateParams;
    $rootScope.$on('$stateChangeStart',
    function(event, toState, toParams, fromState, fromParams){
        $localStorage.history = toState.name;
    });
}])
.config(['$stateProvider', '$urlRouterProvider', 'JQ_CONFIG', function ($stateProvider, $urlRouterProvider, JQ_CONFIG) {
    $urlRouterProvider.otherwise('/access/signin');
    $stateProvider.state('app', {
        abstract: true,
        url: '/app',
        templateUrl: 'views/app.html',
        resolve: {
            auth: auth
        }
    })
    .state('app.dashboard', {
        url: '/dashboard',
        templateUrl: 'views/app_dashboard.html'
    })
    /** 管理 */
    .state('app.system', {
        abstract: true,
        url: '/system',
        template: '<div ui-view class="fade-in-up"></div>'
    })
    
    .state('app.system.metadata', {
        url: '/metadata',
        templateUrl: 'views/system/metadata.html'
    })
    .state('app.system.config', {
        url: '/config',
        templateUrl: 'views/system/config.html'
    })
    .state('app.system.department', {
        url: '/department',
        templateUrl: 'views/system/department.html'
    })
    .state('app.system.user', {
        url: '/user',
        templateUrl: 'views/system/user.html'
    })
    .state('app.system.agent', {
        url: '/agent',
        templateUrl: 'views/system/agent.html'
    })
    /** 系统 */
    .state('app.global', {
        abstract: true,
        url: '/global',
        template: '<div ui-view class="fade-in-up"></div>'
    })
    .state('app.global.global_meta', {
        url: '/global_meta',
        templateUrl: 'views/global/global_meta.html'
    })
    .state('app.global.global_config', {
        url: '/global_config',
        templateUrl: 'views/global/global_config.html'
    })
    .state('app.global.level', {
        url: '/level',
        templateUrl: 'views/global/level.html'
    })
    .state('app.global.permission', {
        url: '/permission',
        templateUrl: 'views/global/permission.html'
    })
    .state('app.global.tenant', {
        url: '/tenant',
        templateUrl: 'views/global/tenant.html'
    })
    /** 工单 */
    .state('app.service', {
        abstract: true,
        url: '/service',
        template: '<div ui-view class="fade-in-up"></div>'
    })
    .state('app.service.service', {
        url: '/service',
        templateUrl: 'views/service/service.html'
    }) 
    .state('app.service.service_edit', {
        url: '/service_edit/:id',
        templateUrl: 'views/service/service_edit.html'
    }) 
    .state('app.service.service_add', {
        url: '/service_add/:type',
        templateUrl: 'views/service/service_add.html'
    })
    .state('app.service.service_axb', {
        url: '/service_axb',
        templateUrl: 'views/service/service_axb_list.html'
    }) 
    .state('app.service.service_axb_edit', {
        url: '/service_axb_edit/:id',
        templateUrl: 'views/service/service_axb_edit.html'
    }) 
    .state('app.service.service_axb_add', {
        url: '/service_axb_add/:type',
        templateUrl: 'views/service/service_axb_add.html'
    }) 
    .state('app.service.service_mxb', {
        url: '/service_mxb',
        templateUrl: 'views/service/service_mxb_list.html'
    }) 
    .state('app.service.service_mxb_edit', {
        url: '/service_mxb_edit/:id',
        templateUrl: 'views/service/service_mxb_edit.html'
    }) 
    .state('app.service.service_mxb_add', {
        url: '/service_mxb_add/:type',
        templateUrl: 'views/service/service_mxb_add.html'
    }) 
    .state('app.service.service_template', {
        url: '/service_template',
        templateUrl: 'views/service/service_template.html'
    }) 
    /** 日常 */
    .state('app.business', {
        abstract: true,
        url: '/business',
        template: '<div ui-view class="fade-in-up"></div>'
    })
    .state('app.business.customer', {
        url: '/customer',
        templateUrl: 'views/business/customer.html'
    })
    /** 个人信息 */
    .state('app.profile', {
        abstract: true,
        url: '/profile',
        template: '<div ui-view class="fade-in-up"></div>'
    })
    .state('app.profile.password', {
        url: '/profile/password',
        templateUrl: 'views/system/profile.html'
    })
    /** 认证 */
    .state('access', {
        abstract: true,
        url: '/access',
        template: '<div ui-view class="fade-in-up"></div>'
    })
    .state('access.signin', {
        url: '/signin',
        templateUrl: 'views/page_signin.html'
    })
    .state('access.signup', {
        url: '/signup',
        templateUrl: 'views/page_signup.html'
    });
    

    function auth(UserService, $state, $timeout) {
        
        //console.log(navigator.userAgent);
        function is_weixn(){
            var ua = navigator.userAgent.toLowerCase();
            if(ua.match(/micromessenger/i)) {
                // 是微信
            } else {
                // 不是微信
            }
        }

        var promise = new Promise(function(resolve, reject){
            UserService.getUser().then(function () {
                if (UserService.user) {
                    resolve();
                } else {
                    reject();
                    $timeout(function () {
                        $state.go('access.signin');
                    });
                }
            });
        });
        return promise;
    }
}]);
