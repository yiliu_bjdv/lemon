/**
 * @fileOverview 数据资源操作服务
 * @author wing ying_gong@bjdv.com
 */
'use strict';
var app = angular.module('app');

var SCHEMA = [
    'user',
    'level',
    'level_permission',
    'permission',
    'tenant',
    'global_meta',
    'global_config',

    'config',
    'metadata',
    'customer',
    'department',

    'service',
    'service_axb',
    'service_mxb',
    'service_log',
    'service_category',
    'service_progress',
    'service_progress_field',

    'announce',
    'blacklist',
    'voice',
    'kb_directory',
    'kb_file',
    'axb_card',
    'product'

];

/**
 * 基础restful配置数据模型
 * config(model_name)后的即可使用模型
 * 分别对应后台
 * GET /api/model_name/:id  => findOne(id, sort, fields)
 * GET /api/model_name?where=where&page=page&limit=limit => findAll(where, sort, fields)
 * PUT /api/model_name/:id {data:model_data}) => save(id, data)
 * POST /api/model_name {data:model_data} => create(data)
 * DELETE /api/model_name/:id => remove(id)
 */
app.factory('$model', function ($myhttp) {
    var service = {};
    service.baseUrl = '/api';
    service.config = function (modelName) {
        var model = {};
        model.findOne = function (id, fields) {
            var url = service.baseUrl + '/' + modelName + '/' + id;
            if (id) {
                return $myhttp.get(url, { options: { attributes: fields } });
            } else {
                return new Promise(function (resolve, reject) {
                    reject(new Error('id为空:' + url));
                });
            }
        };

        model.findAll = function (where, sort, fields) {
            var url = service.baseUrl + '/' + modelName;
            return $myhttp.get(url, { options: { where: where, order: sort, attributes: fields } });
        };

        model.findAllPage = function (where, page, limit, sort, fields) {
            var url = service.baseUrl + '/' + modelName;
            return $myhttp.get(url, { options: { where: where, order: sort, attributes: fields }, page: page, limit: limit });
        };

        model.count = function (where) {
            var url = service.baseUrl + '/' + modelName + '/count';
            return $myhttp.get(url, { where: where });
        };

        model.update = function (data, where) {
            var url = service.baseUrl + '/' + modelName;
            if (where && data) {
                var o = {};
                o['where'] = where;
                o['data'] = data;
                return $myhttp.put(url, o);
            } else {
                return new Promise(function (resolve, reject) {
                    reject(new Error('条件或数据为空:' + url + ',' + JSON.stringify(where) + ',' + JSON.stringify(data)));
                });
            }
        };

        model.save = function (id, data) {
            var url = service.baseUrl + '/' + modelName + '/' + id;
            if (id && data) {
                var o = {};
                o['data'] = data;
                return $myhttp.put(url, o);
            } else {
                return new Promise(function (resolve, reject) {
                    reject(new Error('id或数据为空:' + url + ',' + JSON.stringify(data)));
                });
            }
        };

        model.create = function (data) {
            var url = service.baseUrl + '/' + modelName;
            if (data) {
                var o = {};
                o['data'] = data;
                return $myhttp.post(url, o);
            } else {
                return new Promise(function (resolve, reject) {
                    reject(new Error('数据为空:' + url + ',' + JSON.stringify(data)));
                });
            }
        };

        model.upsert = function (data, where) {
            var url = service.baseUrl + '/' + modelName;
            if (data) {
                var o = {};
                o['data'] = data;
                o['where'] = where;
                return $myhttp.post(url, o);
            } else {
                return new Promise(function (resolve, reject) {
                    reject(new Error('数据为空:' + url + ',' + JSON.stringify(data)));
                });
            }
        };

        model.remove = function (id) {
            var url = service.baseUrl + '/' + modelName + '/' + id;
            if (id) {
                return $myhttp.del(url);
            } else {
                return new Promise(function (resolve, reject) {
                    reject(new Error('id为空:' + url));
                });
            }
        };
        return model;
    };

    return service;
});

/** 访问模型 */
app.factory('$access', function ($myhttp) {
    var model = {};
    model.login = function (data) {
        data.passwd = undefined;
        return $myhttp.post('/access/login', { data: data });
    };

    model.logout = function () {
        return $myhttp.post('/access/logout');
    };

    model.signup = function (data) {
        data.passwd = undefined;
        return $myhttp.post('/access/signup', { data: data });
    };
    return model;
});

/** 默认初始化SCHEMA数组中的表 */
app.run(function ($model) {
    _.each(SCHEMA, function (v) {
        app.factory('$' + v, function () {
            var model = $model.config(v);
            return model;
        });
    });
});

/** 全局数据字典 */
app.run(function ($global_meta, $myhttp) {
    $global_meta.categories = function () {
        return $myhttp.get('/api/global_meta/category');
    };
    $global_meta.category = function (key) {
        return $myhttp.get('/api/global_meta/category/' + key);
    };
    var oriFindOne = $global_meta.findOne;
    var oriFindAll = $global_meta.findAll;
    var oriFindAllPage = $global_meta.findAllPage;
    $global_meta.findOne = function (id, fields) {
        var meta;
        return oriFindOne(id, fields).then(function (r) {
            var meta = r;
            return $global_meta.category(meta.key);
        }).then(function (category) {
            if (category.type == 'number') {
                meta.value = _.toNumber(meta.value);
            }
            return meta;
        });
    };
    $global_meta.findAll = function (where, sort, fields) {
        var metas;
        return oriFindAll(where, sort, fields).then(function (r) {
            metas = r;
            if(where.key){
                return $global_meta.category(where.key);
            }else{
                return $global_meta.categories();
            }
        }).then(function (category) {
            if (category.type == 'number') {
                _.each(metas, function (meta) {
                    meta.value = _.toNumber(meta.value);
                });
            }
            return metas;
        });
    };
    $global_meta.findAllPage = function (where, page, limit, sort, fields) {
        var result;
        return oriFindAllPage(where, page, limit, sort, fields).then(function (r) {
            result = r;
            return $global_meta.category(where.key);
        }).then(function (category) {
            if (category.type == 'number') {
                _.each(result.rows, function (meta) {
                    meta.value = _.toNumber(meta.value);
                });
            }
            return result;
        });
    };
});

app.factory('$area_street', function ($myhttp) {
    var service = {};
    service.findAll = function (code) {
        return $myhttp.get('/api/area_street', { code: code });
    };
    return service;
});

/** 全局配置 */
app.run(function ($global_config, $myhttp) {
    $global_config.categories = function () {
        return $myhttp.get('/api/global_config/category');
    };
});


/** 租户数据字典 */
app.run(function ($metadata, $myhttp) {
    $metadata.categories = function () {
        return $myhttp.get('/api/metadata/category');
    };
    $metadata.category = function (key) {
        return $myhttp.get('/api/metadata/category/' + key);
    };
    var oriFindOne = $metadata.findOne;
    var oriFindAll = $metadata.findAll;
    var oriFindAllPage = $metadata.findAllPage;
    $metadata.findOne = function (id, fields) {
        var meta;
        return oriFindOne(id, fields).then(function (r) {
            var meta = r;
            return $metadata.category(meta.key);
        }).then(function (category) {
            if (category && category.type == 'number') {
                meta.value = _.toNumber(meta.value);
            }
            return meta;
        });
    };
    $metadata.findAll = function (where, sort, fields) {
        var metas;
        return oriFindAll(where, sort, fields).then(function (r) {
            metas = r;
            return $metadata.category(where.key);
        }).then(function (category) {
            if (category && category.type == 'number') {
                _.each(metas, function (meta) {
                    meta.value = _.toNumber(meta.value);
                });
            }
            return metas;
        });
    };
    $metadata.findAllPage = function (where, page, limit, sort, fields) {
        var result;
        return oriFindAllPage(where, page, limit, sort, fields).then(function (r) {
            result = r;
            return $metadata.category(where.key);
        }).then(function (category) {
            if (category && category.type == 'number') {
                _.each(result.rows, function (meta) {
                    meta.value = _.toNumber(meta.value);
                });
            }
            return result;
        });
    };
    $metadata.findUser = function(){
        return $myhttp.get('/api/metadata/user');
    };
});



/** 租户配置 */
app.run(function ($config, $myhttp) {
    $config.categories = function () {
        return $myhttp.get('/api/config/category');
    };
});

/** 租户配置 */
app.run(function ($level, $level_permission) {
    var oriRemove = $level.remove;
    $level.remove = function (id) {
        return $level_permission.findAll({ level_id: id }).each(function (level_permission) {
            return $level_permission.remove(level_permission.id);
        }).then(function () {
            return oriRemove(id);
        });
    };
});

/** 部门模型 */
app.run(function ($department, $myhttp) {
    $department.create = function (data, parent) {
        if (_.isUndefined(parent)) {
            return $myhttp.post('/api/department', { data: data });
        } else {
            return $myhttp.post('/api/department/' + parent, { data: data });
        }
    };
    $department.leaf = function (parent) {
        return $myhttp.get('/api/department/leaf/' + parent);
    };
    $department.son = function (parent) {
        return $myhttp.get('/api/department/son/' + parent);
    };
    delete $department.upsert;
    delete $department.update;
    return $department;
});

/** 
 * 工单分类进度关系资源
 * @author wing ying_gong@bjdv.com
 */
app.factory('$service_category_progress', function ($myhttp) {
    var service = {};
    service.findAll = function (params) {
        return $myhttp.get('/api/service_category_progress', params);
    };
    service.update = function (categoryId, progresses) {
        return $myhttp.post('/api/service_category_progress/' + categoryId, { data: progresses });
    };
    return service;
});

/**
 * 工单资源
 */
app.run(function($service){
    delete $service.update;
    delete $service.upsert;
});


app.run(function($tenant, $myhttp){
    $tenant.admin = function(user){
        return $myhttp.post('/api/tenant/admin', {data: user});
    };
});

