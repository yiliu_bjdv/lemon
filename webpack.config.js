var webpack = require('webpack');

module.exports = {
    entry: {
        client: './client/app_entry.js'
    },
    output: {
        path: __dirname + '/client/build',
        filename: 'app_bundle.js'
    },
    externals: {
        jquery: 'jQuery',
        angular: 'angular',
        moment: 'moment',
        Promise: 'bluebird',
        _: 'lodash',
        go: 'go',
        ace: 'ace'
    },
    devtool: 'source-map',
    module: {
        loaders: [
            /** style */
            { test: /\.css$/, loader: "style!css!" },
            { test: /\.less$/, loader: "style!css!less!" },
            { test: /\.(jpg|png|gif|svg)$/, loader: "url?limit=10000" },

            /** html */
            { test: /\.html$/, loader: "html" },

            //bootstrap
            {test: /\.woff($|\?)|\.woff2($|\?)|\.ttf($|\?)|\.eot($|\?)/,loader: 'url'}

            /*
            //font-awesome
            { test: /\.woff(2)?(\?v=[0-9]\.[0-9]\.[0-9])?$/, loader: "url-loader?limit=10000&minetype=application/font-woff" },
            { test: /\.(ttf|eot|svg)(\?v=[0-9]\.[0-9]\.[0-9])?$/, loader: "file-loader" }*/
        ]
    }
};