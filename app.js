/**
 * @fileOverview 后端主程序
 * @author wing ying_gong@bjdv.com
 */
var path = require('path');
var fs = require('fs');
var http = require('http');
var log4js = require('log4js');

process.env.PORT = 8080;

var jwtSecret = 'webseed';
process.env.JWT_SECRET = jwtSecret;
process.env.JWT_EXPIRE = "12h";

/** log4js initiation */
if (!fs.existsSync(path.join(__dirname, 'logs'))) {
    fs.mkdirSync(path.join(__dirname, 'logs'));
}

log4js.configure('./server/config/log4js.json');
var logger = log4js.getLogger('app');

if (!fs.existsSync(path.join(__dirname, 'uploads'))) {
    fs.mkdirSync(path.join(__dirname, 'uploads'));
}
if (!fs.existsSync(path.join(__dirname, 'resources'))) {
    fs.mkdirSync(path.join(__dirname, 'resources'));
}

/** express */
var express = require('express');
var favicon = require('serve-favicon');

/** jwt */
var expressJwt = require('express-jwt'); 

var bodyParser = require('body-parser');

var app = express();

var httpServer = http.createServer(app);
var io = require('socket.io')(httpServer);
app.use(log4js.connectLogger(log4js.getLogger("http"),  { level: 'auto', nolog: [
    '\\.css', '\/js\/', '\\.html', '\/img\/', '\/fonts\/', '\/bower_components\/', '\/l10n\/'
] }));
app.use(favicon(__dirname + '/client/favicon.ico'));
app.use(bodyParser.json({limit: '50mb'}));
app.use(bodyParser.urlencoded({limit: '50mb', extended: false }));

app.use('/api', expressJwt({secret: jwtSecret}));
app.use(express.static(path.join(__dirname, 'client')));
app.use('/node_modules', express.static(path.join(__dirname, 'node_modules')));

require('./server')(app);
require('./server/io').route(io);

// catch 404 and forward to error handler
app.use(function(req, res, next) {
    var err = new Error( req.url + ' 页面找不到');
    err.status = 404;
    res.send(JSON.stringify({
        err: err.message,
        message: err.message
    }));
    logger.warn(err.message);
});

// error handlers
app.use(function (err, req, res) {
    res.status(err.status || 500);
    res.send(JSON.stringify({
        err: err.message,
        message: req.url + " " + err.message,
        error: err
    }));
    logger.warn(err.stack);
});

httpServer.listen(process.env.PORT, function() {
    logger.info('app 启动监听' + httpServer.address().port);
});