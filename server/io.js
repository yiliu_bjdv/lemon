let jwt = require('jsonwebtoken');
let logger = require('log4js').getLogger('io');
let JWT_SECRET = process.env.JWT_SECRET;
let iosocket;
exports.route = (io) => {
    iosocket = io;
    io.on('connection', (socket) => {
        socket.on('register', (msg) => {
            try {
                let o = jwt.verify(msg.token, JWT_SECRET);
                if (o.tenant_id && o.id) {
                    socket.join(`tenant_${o.tenant_id}`);
                    socket.join(`user_${o.id}`);
                    logger.info(`${socket.id} join`, o.tenant_id, o.id);
                }
            } catch (e) {
                logger.warn('web事件socket无权限', msg, e.message);
            }
        });

        socket.on('unregister', (msg) => {
            try {
                let o = jwt.verify(msg.token, JWT_SECRET);
                if (o.tenant_id && o.id) {
                    socket.leave(`tenant_${o.tenant_id}`);
                    socket.leave(`user_${o.id}`);
                    logger.info(`${socket.id} leave`, o.tenant_id, o.id);
                }
            } catch (e) {
                logger.warn('web事件socket无权限', msg, e.message);
            }
        });
    });
};

exports.emitToTenant = (id, evt, msg) => {
    if (iosocket && id) {
        logger.info('emit to tenant', id, evt, msg);
        iosocket.to(`tenant_${id}`).emit('evt', { evt, msg, scope: 'tenant' });
    }
};

exports.emitToUser = (id, evt, msg) => {
    if (iosocket && id) {
        logger.info('emit to user', id, evt, msg);
        iosocket.to(`user_${id}`).emit('evt', { evt, msg, scope: 'user' });
    }
};


