exports.new = (o) => {
    if (!o) {
        o = exports.RES.UNKNOWN;
    }
    var err = new Error(o.message);
    err.res = o.res;
    return err;
};

exports.RES = {
    UNKNOWN: { res: 800, message: '未知异常' },
    TABLE_NOTEXIST: { res: 801, message: '表不存在' },
    DATA_ERR: {res: 802, message: '数据有误'},
    AGENT_NOTEXIST: {res: 803, message: '认证用户不存在'},
    AGENT_DISABLED: {res: 804, message: '认证用户已停用'},
    AGENT_PASSWORD_ERR: {res: 805, message: '认证密码有误'},
    AGENT_EXPIRE_ERR: {res: 806, message: '认证过期时间有误'},
    DEPARTMENT_PARENT_ERR: {res: 820, message: '上级部门有误'},
    DEPARTMENT_PARENT_CANNOT_MODIFY: {res:821, message: '根部门不能修改'},
    DEPARTMENT_CANNOT_BELONG_SELF_SON: {res: 822, message: '部门不能挂在自己或者子部门下'},
    DEPARTMENT_CANNOT_DELETE_HAS_SON: {res:823, message: '不能删除还有子部门的部门'},
    DEPARTMENT_ERR: {res: 824, message:'部门id有误'},
    INCTABLE_DATE_ERR: {res: 825, message:'增量库日期有误'}
};