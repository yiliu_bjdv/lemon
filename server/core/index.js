const redisConf = require('../config/redis');
const Client = require('vcc2core').Client;
var Promise = require('bluebird');

var rc = exports.rc = new Client(redisConf.self);
var grc = exports.grc = new Client(redisConf.global);

rc.on('connect', ()=>{
    
});

rc.on('error', ()=>{

});

grc.on('connect', ()=>{
    
});

grc.on('error', ()=>{

});