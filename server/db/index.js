
var Sequelize = require('sequelize');
var moment = require('moment');
var _ = require('lodash');
var Promise = require('bluebird');
var dbConfig = require('../config/db');
var error = require('../error').error;
var CAUSE = require('../error').CAUSE;

Date.prototype.toJSON = function () {
    return moment(this).format('YYYY-MM-DD HH:mm:ss');
};

var sequelize_vcc = new Sequelize(dbConfig.database, dbConfig.user, dbConfig.password, {
    host: dbConfig.host,
    dialect: 'mysql',
    pool: dbConfig.pool,
    timezone: '+08:00'
});

var SCHEMA = require('./schema').SCHEMA;
var GLOBALTABLE = require('./schema').GLOBALTABLE;

var definedModel = {};
function model(modelName) {
    return Promise.resolve().then(() => {
        var Model = SCHEMA[modelName];
        if (!Model) {
            throw error(CAUSE.TABLE_NOTEXIST);
        }
        var model = definedModel[modelName];
        if (!model) {
            model = sequelize_vcc.define(modelName, Model, {
                timestamps: false,
                freezeTableName: true
            });
            definedModel[modelName] = model;
        }
        return model;
    });
}

function pagination(count, curPage, limit) {
    limit = Number(limit || 10);
    curPage = Number(curPage || 1);
    var r = { count: count };
    r.total = Math.floor(count / limit) + 1;
    if (curPage <= 0) {
        r.page = 1;
    } else if (curPage > r.total) {
        r.page = r.total;
    } else {
        r.page = curPage;
    }
    r.skip = (r.page - 1) * limit;
    r.limit = limit;
    return r;
}

function parse(o) {
    var r = {};
    try {
        if (_.isObject(o)) {
            r = o;
        } else {
            r = JSON.parse(o);
        }
    } catch (e) {
        r = {};
    }
    return r;
}

exports.userAuth = function (userName) {
    return model('user').then((Model) => {
        return Model.findOne({ where: { user_name: userName } });
    });
};

exports.userPermission = function (level) {
    return Promise.resolve().then(function () {
        var sql = "select p.permission " +
            " from level_permission lp, permission p, level l" +
            " where lp.permission_id=p.id and lp.level_id=l.id and l.level=:level";
        return sequelize_vcc.query(sql, {
            replacements: {
                level: level
            },
            type: Sequelize.QueryTypes.SELECT
        });
    });
};

exports.tenantAdmin = (optBy, admin) => {
    return model('user').then((Model) => {
        admin.opt_by = optBy;
        admin.level = 300;
        admin.status = 1;
        admin.create_at = new Date();
        admin.update_at = new Date();
        return Model.create(admin);
    });
};

/** 配置库操作类 */
class Dbl {
    constructor(tenant_id, user_id) {
        this.tenant_id = tenant_id;
        this.user_id = user_id;
    }

    isGlobal(tableName) {
        return _.includes(GLOBALTABLE, tableName);
    }

    findOne(tableName, id, options) {
        var self = this;
        return model(tableName).then((Model) => {
            var where = { id: id, tenant_id: self.tenant_id };
            options = parse(options);
            options.where = where;
            if (self.isGlobal(tableName)) {
                where = { id: id };
            }
            return Model.findOne(options);
        });
    }

    findAll(tableName, options) {
        var self = this;
        return model(tableName).then((Model) => {
            options = parse(options);
            if (!self.isGlobal(tableName)) {
                options.where = options.where || {};
                options.where.tenant_id = self.tenant_id;
            }
            return Model.findAll(options);
        });
    }

    findAllPage(tableName, options, page, limit) {
        var self = this;
        var Model;
        var pr;
        return model(tableName).then((r) => {
            options = parse(options);
            if (!self.isGlobal(tableName)) {
                options.where = options.where || {};
                options.where.tenant_id = self.tenant_id;
            }
            Model = r;
            return Model.count({ where: options.where });
        }).then((count) => {
            pr = pagination(count, page, limit);
            return Model.findAll(options);
        }).then((rows) => {
            pr.rows = rows;
            return pr;
        });
    }

    //工单查询
    findAllServicePage(options, page, limit) {
        var self = this;
        return Promise.resolve().then(function () {
            var params = parse(options);
            var sql = "select s.* " +
                " from service s,customer c" +
                " where s.customer_id = c.id and s.tenant_id=:tenant_id and s.tenant_id = c.tenant_id";
            if (params.where.content) {
                sql += " and (c.name like :content or c.email like :content)";
            }
            return sequelize_vcc.query(sql, {
                replacements: {
                    tenant_id: self.tenant_id,
                    content: params.where.content
                },
                type: Sequelize.QueryTypes.SELECT
            });
        });
    }


    count(tableName, options) {
        var self = this;
        return model(tableName).then((Model) => {
            options = parse(options);
            if (!self.isGlobal(tableName)) {
                options.where = options.where || {};
                options.where.tenant_id = self.tenant_id;
            }
            return Model.count(options);
        });
    }

    create(tableName, o) {
        var self = this;
        return model(tableName).then((Model) => {
            if (_.isObject(o)) {
                if (!self.isGlobal(tableName)) {
                    o.tenant_id = self.tenant_id;
                }
                o.opt_by = self.user_id;
                o.create_at = new Date();
                o.update_at = new Date();
                return Model.create(o);
            } else {
                throw error(CAUSE.DATA_ERR);
            }
        });
    }

    update(tableName, o, where) {
        var self = this;
        var options = {};
        return model(tableName).then((Model) => {
            options.where = parse(where);
            if (_.isObject(o)) {
                if (!self.isGlobal(tableName)) {
                    o.tenant_id = self.tenant_id;
                    options.where.tenant_id = self.tenant_id;
                }
                o.opt_by = self.user_id;
                o.update_at = new Date();
                return Model.update(o, options);
            } else {
                throw error(CAUSE.DATA_ERR);
            }
        });
    }

    upsert(tableName, o, where) {
        var self = this;
        var w = _.clone(where);
        return Promise.resolve().then(() => {
            return self.count(tableName, { where: where });
        }).then((count) => {
            if (count > 0) {
                return self.update(tableName, o, w);
            } else {
                return self.create(tableName, o);
            }
        });
    }

    remove(tableName, id) {
        var self = this;
        return model(tableName).then((Model) => {
            var where = {
                id: id,
                tenant_id: self.tenant_id
            };
            if (self.isGlobal(tableName)) {
                where = { id: id };
            }
            return Model.destroy({ where: where });
        });
    }

    findCategoryProgress(params) {
        var self = this;
        return Promise.resolve().then(function () {
            params = parse(params);
            var sql = "select cp.id, cp.progress_id, cp.category_id, c.name category_name, p.name progress_name " +
                " from service_category_progress cp,service_category c, service_progress p" +
                " where cp.category_id = c.id and cp.progress_id=p.id and cp.tenant_id=:tenant_id and c.status = 1";
            if (params.progress_id) {
                sql += " and progress_id=:progress_id";
            } else if (params.category_id) {
                sql += " and category_id=:category_id";
            } else {
                return [];
            }
            return sequelize_vcc.query(sql, {
                replacements: {
                    tenant_id: self.tenant_id,
                    category_id: params.category_id,
                    progress_id: params.progress_id
                },
                type: Sequelize.QueryTypes.SELECT
            });
        });
    }

    updateCategoryProgress(categoryId, progresses) {
        var self = this;
        var Model;
        return model('service_category_progress').then(function (r) {
            Model = r;
            return Model.destroy({
                where: {
                    tenant_id: self.tenant_id,
                    category_id: categoryId
                }
            }).then(function () {
                return progresses;
            }).each(function (progressId) {
                var cp = {};
                cp.category_id = categoryId;
                cp.progress_id = progressId;
                cp.tenant_id = self.tenant_id;
                cp.opt_by = self.user_id;
                cp.create_at = new Date();
                cp.update_at = new Date();
                return Model.create(cp);
            });
        });
    }

    findStreet(code = "") {
        return model('area_street').then(function (Model) {
            if (code.length < 2) {
                return Model.findAll({ where: { code: { $like: code + '%' }, level: 1 }, sort: ['code'], attributes: ['code', 'name'] });
            } else if (code.length == 2) {
                return Model.findAll({ where: { code: { $like: code + '%' }, level: 2 }, sort: ['code'], attributes: ['code', 'name'] });
            } else if (code.length == 4) {
                return Model.findAll({ where: { code: { $like: code + '%' }, level: 3 }, sort: ['code'], attributes: ['code', 'name'] });
            } else if (code.length == 6) {
                return Model.findAll({ where: { code: { $like: code + '%' }, level: 4 }, sort: ['code'], attributes: ['code', 'name'] });
            } else {
                return [];
            }
        });
    }


    
    //工单轨迹
    saveAudit(service,action_code,remark,content) {
        var self = this;
        var service_old = {};
        return model('service').then((Model) => {
            var where = { id: service.id, tenant_id: self.tenant_id };
            var options = {};
            options.where = where;
            return Model.findOne(options);
        }).then(function (result) {
                if(result){
                    service_old = result;
                }
            }).then(function(){
                return model('service_log').then((Model) => {
                    var o = {};
                    if (_.isObject(o)) {
                        if (!self.isGlobal('service_log')) {
                            o.tenant_id = self.tenant_id;
                        }
                        o.opt_by = self.user_id;
                        o.record_at = new Date();
                        o.action_code = action_code;
                        o.service_id = service.id;
                        o.remark = remark;
                        o.content = content;
                        o.start_state = service_old.progress_id;
                        o.end_state = service.progress_id; 
                        
                        var audit = [];
                        //变更审计字段
                        if(service_old.deal_by!=service.deal_by){
                            audit.push({
                                old:service_old.deal_by,
                                new:service.deal_by,
                                context:'deal_by'
                            })
                        }
                        if(service_old.street!=service.street){
                            audit.push({
                                old:service_old.street,
                                new:service.street,
                                context:'street'
                            })
                        }
                        if(service_old.address!=service.address){
                            audit.push({
                                old:service_old.address,
                                new:service.address,
                                context:'address'
                            })
                        }
                        if(service_old.extra!=service.extra){
                            audit.push({
                                old:service_old.extra,
                                new:service.extra,
                                context:'extra'
                            })
                        }
                        if(service_old.fee!=service.fee){
                            audit.push({
                                old:service_old.fee,
                                new:service.fee,
                                context:'fee'
                            })
                        }
                        if(service_old.service_type!=service.service_type){
                            audit.push({
                                old:service_old.service_type,
                                new:service.service_type,
                                context:'service_type'
                            })
                        }
                        if(service_old.dispatch_by!=service.dispatch_by){
                            audit.push({
                                old:service_old.dispatch_by,
                                new:service.dispatch_by,
                                context:'dispatch_by'
                            })
                        }

                        o.audit  = JSON.stringify(audit);
                        return Model.create(o);
                    } else {
                        throw error.new(error.RES.DATA_ERR);
                    }
                });
            })
    }
}

/** 导出配置库操作类 */
exports.Dbl = Dbl;
