var Sequelize = require('sequelize');

/** 全局库 */
exports.GLOBALTABLE = [
    'global_meta',
    'global_config',
    'level',
    'level_permission',
    'permission',
    'area_code',
    'area_street',
    'tenant'
];

/** 配置表定义 */
var SCHEMA = {};
SCHEMA.user = {
    id: { type: Sequelize.INTEGER, primaryKey: true, autoIncrement: true },
    tenant_id: { type: Sequelize.INTEGER, allowNull: false },
    user_name: { type: Sequelize.STRING(50), allowNull: false, unique: true },
    name: { type: Sequelize.STRING(50), allowNull: false },
    password: { type: Sequelize.STRING(32), allowNull: false },
    status: { type: Sequelize.INTEGER(4), allowNull: false },
    level: { type: Sequelize.INTEGER, allowNull: false },
    tel: { type: Sequelize.STRING(30), allowNull: true },
    email: { type: Sequelize.STRING(30), allowNull: true },
    sex: { type: Sequelize.STRING(30), allowNull: true },
    department_id: { type: Sequelize.INTEGER, allowNull: true },
    opt_by: { type: Sequelize.INTEGER, allowNull: false },
    create_at: { type: Sequelize.DATE },
    update_at: { type: Sequelize.DATE }
};

SCHEMA.area_code = {
    code: { type: Sequelize.STRING(4), primaryKey: true },
    city: { type: Sequelize.STRING(50), allowNull: false }
};

SCHEMA.area_street = {
    id: { type: Sequelize.INTEGER, primaryKey: true, autoIncrement: true },
    code: { type: Sequelize.STRING(11), allowNull: false },
    parent: { type: Sequelize.STRING(11), allowNull: false },
    name: { type: Sequelize.STRING(50), allowNull: false },
    level: { type: Sequelize.INTEGER(4), allowNull: false }
};

SCHEMA.global_config = {
    id: { type: Sequelize.INTEGER, primaryKey: true, autoIncrement: true },
    key: { type: Sequelize.STRING(50), allowNull: false },
    context: { type: Sequelize.STRING(255), allowNull: false },
    value: { type: Sequelize.STRING(255), allowNull: false },
    status: { type: Sequelize.INTEGER(4), allowNull: false },
    opt_by: { type: Sequelize.INTEGER, allowNull: false },
    create_at: { type: Sequelize.DATE },
    update_at: { type: Sequelize.DATE }
};

SCHEMA.global_meta = {
    id: { type: Sequelize.INTEGER, primaryKey: true, autoIncrement: true },
    key: { type: Sequelize.STRING(50), allowNull: false },
    context: { type: Sequelize.STRING(50), allowNull: false },
    source: { type: Sequelize.STRING(50), allowNull: true },
    ref_key: { type: Sequelize.STRING(50), allowNull: true },
    value: { type: Sequelize.TEXT, allowNull: false },
    status: { type: Sequelize.INTEGER(4), allowNull: false },
    opt_by: { type: Sequelize.INTEGER, allowNull: false },
    create_at: { type: Sequelize.DATE },
    update_at: { type: Sequelize.DATE }
};

SCHEMA.level = {
    id: { type: Sequelize.INTEGER, primaryKey: true, autoIncrement: true },
    level: { type: Sequelize.INTEGER, unique: true, allowNull: false },
    name: { type: Sequelize.STRING(50), allowNull: false },
    opt_by: { type: Sequelize.INTEGER, allowNull: false },
    create_at: { type: Sequelize.DATE },
    update_at: { type: Sequelize.DATE }
};

SCHEMA.level_permission = {
    id: { type: Sequelize.INTEGER, primaryKey: true, autoIncrement: true },
    level_id: { type: Sequelize.INTEGER, allowNull: false },
    permission_id: { type: Sequelize.INTEGER, allowNull: false },
    opt_by: { type: Sequelize.INTEGER, allowNull: false },
    create_at: { type: Sequelize.DATE },
    update_at: { type: Sequelize.DATE }
};

SCHEMA.permission = {
    id: { type: Sequelize.INTEGER, primaryKey: true, autoIncrement: true },
    permission: { type: Sequelize.STRING(50), unique: true, allowNull: false },
    name: { type: Sequelize.STRING(50), allowNull: false },
    opt_by: { type: Sequelize.INTEGER, allowNull: false },
    create_at: { type: Sequelize.DATE },
    update_at: { type: Sequelize.DATE }
};

SCHEMA.customer = {
    id: { type: Sequelize.INTEGER, primaryKey: true, autoIncrement: true },
    tenant_id: { type: Sequelize.INTEGER, allowNull: false },
    name: { type: Sequelize.STRING(50), allowNull: false },
    tel: { type: Sequelize.STRING(20), allowNull: false },
    mobile: { type: Sequelize.STRING(20), allowNull: false },
    email: { type: Sequelize.STRING(20), allowNull: true },
    sex: { type: Sequelize.STRING(20), allowNull: true },
    level: { type: Sequelize.STRING(20), allowNull: true },
    identity: { type: Sequelize.STRING(20), allowNull: true },
    company: { type: Sequelize.STRING(20), allowNull: true },
    qq: { type: Sequelize.STRING(20), allowNull: true },
    street: { type: Sequelize.STRING(11), allowNull: true },
    address: { type: Sequelize.STRING(255), allowNull: true },
    opt_by: { type: Sequelize.INTEGER, allowNull: false },
    create_at: { type: Sequelize.DATE },
    update_at: { type: Sequelize.DATE },
    state: { type: Sequelize.STRING(20), allowNull: true }
};

SCHEMA.department = {
    id: { type: Sequelize.INTEGER, primaryKey: true, autoIncrement: true },
    tenant_id: { type: Sequelize.INTEGER, allowNull: false },
    name: { type: Sequelize.STRING(50), allowNull: false },
    parent: { type: Sequelize.INTEGER, allowNull: true },
    ancestors: { type: Sequelize.STRING, allowNull: true },
    depth: { type: Sequelize.INTEGER(4), allowNull: false },
    opt_by: { type: Sequelize.INTEGER, allowNull: false },
    create_at: { type: Sequelize.DATE },
    update_at: { type: Sequelize.DATE },
    tel: { type: Sequelize.STRING, allowNull: true },
    street: { type: Sequelize.INTEGER, allowNull: true },
    address: { type: Sequelize.STRING, allowNull: true },
    state: { type: Sequelize.STRING, allowNull: true }
};

SCHEMA.config = {
    id: { type: Sequelize.INTEGER, primaryKey: true, autoIncrement: true },
    tenant_id: { type: Sequelize.INTEGER, allowNull: false },
    key: { type: Sequelize.STRING(50), allowNull: false },
    context: { type: Sequelize.STRING(255), allowNull: false },
    value: { type: Sequelize.STRING(255), allowNull: false },
    status: { type: Sequelize.INTEGER(4), allowNull: false },
    opt_by: { type: Sequelize.INTEGER, allowNull: false },
    create_at: { type: Sequelize.DATE },
    update_at: { type: Sequelize.DATE }
};

SCHEMA.metadata = {
    id: { type: Sequelize.INTEGER, primaryKey: true, autoIncrement: true },
    tenant_id: { type: Sequelize.INTEGER, allowNull: false },
    key: { type: Sequelize.STRING(50), allowNull: false },
    context: { type: Sequelize.STRING(50), allowNull: false },
    value: { type: Sequelize.TEXT, allowNull: false },
    status: { type: Sequelize.INTEGER(4), allowNull: false },
    opt_by: { type: Sequelize.INTEGER, allowNull: false },
    create_at: { type: Sequelize.DATE },
    update_at: { type: Sequelize.DATE }
};

SCHEMA.tenant = {
    id: { type: Sequelize.INTEGER, primaryKey: true, autoIncrement: true },
    name: { type: Sequelize.STRING(50), allowNull: false, unique: true },
    alias: { type: Sequelize.STRING(50), allowNull: false },
    status: { type: Sequelize.INTEGER(4), allowNull: false },
    opt_by: { type: Sequelize.INTEGER, allowNull: false },
    create_at: { type: Sequelize.DATE },
    update_at: { type: Sequelize.DATE }
};

SCHEMA.service_category = {
    id: { type: Sequelize.INTEGER, primaryKey: true, autoIncrement: true },
    tenant_id: { type: Sequelize.INTEGER, allowNull: false },
    name: { type: Sequelize.STRING(50), allowNull: false },
    status: { type: Sequelize.INTEGER(4), allowNull: false },
    opt_by: { type: Sequelize.INTEGER, allowNull: false },
    create_at: { type: Sequelize.DATE },
    update_at: { type: Sequelize.DATE }
};

SCHEMA.service_category_progress = {
    id: { type: Sequelize.INTEGER, primaryKey: true, autoIncrement: true },
    tenant_id: { type: Sequelize.INTEGER, allowNull: false },
    category_id: { type: Sequelize.INTEGER, allowNull: false },
    progress_id: { type: Sequelize.INTEGER, allowNull: false },
    opt_by: { type: Sequelize.INTEGER, allowNull: false },
    create_at: { type: Sequelize.DATE },
    update_at: { type: Sequelize.DATE }
};

SCHEMA.service_progress = {
    id: { type: Sequelize.INTEGER, primaryKey: true, autoIncrement: true },
    tenant_id: { type: Sequelize.INTEGER, allowNull: false },
    name: { type: Sequelize.STRING(50), allowNull: false },
    seq: { type: Sequelize.INTEGER(6), allowNull: false },
    status: { type: Sequelize.INTEGER(4), allowNull: false },
    opt_by: { type: Sequelize.INTEGER, allowNull: false },
    create_at: { type: Sequelize.DATE },
    update_at: { type: Sequelize.DATE }
};

SCHEMA.service_progress_field = {
    id: { type: Sequelize.INTEGER, primaryKey: true, autoIncrement: true },
    tenant_id: { type: Sequelize.INTEGER, allowNull: false },
    progress_id: { type: Sequelize.INTEGER, allowNull: false },
    name: { type: Sequelize.STRING(50), allowNull: false },
    field_type: { type: Sequelize.STRING(10), allowNull: false },
    seq: { type: Sequelize.INTEGER(6), allowNull: false },
    status: { type: Sequelize.INTEGER(4), allowNull: false },
    opt_by: { type: Sequelize.INTEGER, allowNull: false },
    create_at: { type: Sequelize.DATE },
    update_at: { type: Sequelize.DATE }
};

SCHEMA.service = {
    id: { type: Sequelize.INTEGER, primaryKey: true, autoIncrement: true },
    tenant_id: { type: Sequelize.INTEGER, allowNull: false },
    state_id: { type: Sequelize.INTEGER, allowNull: false },
    progress_id: { type: Sequelize.INTEGER, allowNull: false },
    customer_id: { type: Sequelize.STRING, allowNull: true },
    street: { type: Sequelize.STRING(11), allowNull: true },
    address: { type: Sequelize.STRING(255), allowNull: true },
    extra: {type: Sequelize.TEXT, allowNull: true},
    dispatch_by: { type: Sequelize.INTEGER, allowNull: true }, //派单人
    deal_by: { type: Sequelize.INTEGER, allowNull: true }, //处理人
    opt_by: { type: Sequelize.INTEGER, allowNull: false },
    create_at: { type: Sequelize.DATE },
    update_at: { type: Sequelize.DATE },
    service_type: { type: Sequelize.STRING(11), allowNull: false }
};

SCHEMA.service_log = {
    id: { type: Sequelize.INTEGER, primaryKey: true, autoIncrement: true },
    tenant_id: { type: Sequelize.INTEGER, allowNull: false },
    service_id: { type: Sequelize.INTEGER, allowNull: false },
    start_state: { type: Sequelize.STRING, allowNull: false },
    end_state: { type: Sequelize.STRING, allowNull: false },
    remark: { type: Sequelize.STRING, allowNull: true },
    content: { type: Sequelize.STRING(255), allowNull: true },
    action_code: { type: Sequelize.STRING(255), allowNull: true },
    audit: {type: Sequelize.TEXT, allowNull: true},
    opt_by: { type: Sequelize.INTEGER, allowNull: false },
    record_at: { type: Sequelize.DATE }
};

SCHEMA.service_mxb = {
    id: { type: Sequelize.INTEGER, primaryKey: true, autoIncrement: true },
    tenant_id: { type: Sequelize.INTEGER, allowNull: false },
    department_id: { type: Sequelize.INTEGER, allowNull: false },
    product_id: { type: Sequelize.INTEGER, allowNull: false },
    customer_id: { type: Sequelize.STRING, allowNull: true },
    state: { type: Sequelize.STRING(11), allowNull: true },//Y有效
    type: { type: Sequelize.STRING(255), allowNull: true },//延保政策
    product_price: { type: Sequelize.INTEGER, allowNull: true },//商品价格
    price: { type: Sequelize.INTEGER, allowNull: true }, //延保费用
    extra: {type: Sequelize.TEXT, allowNull: true},
    deal_by: { type: Sequelize.INTEGER, allowNull: true }, //处理人
    opt_by: { type: Sequelize.INTEGER, allowNull: false },
    create_at: { type: Sequelize.DATE },
    update_at: { type: Sequelize.DATE },
    life: { type: Sequelize.STRING(11), allowNull: true },//延保年限
    product_sn: { type: Sequelize.STRING(11), allowNull: true },//机器编码
    sn: { type: Sequelize.STRING(11), allowNull: true }, //对应axb的code
    invoice: { type: Sequelize.STRING(11), allowNull: true },
    comment: { type: Sequelize.STRING(11), allowNull: true },
    buy_at: Date,
    sell_at: Date,
    machine_begin_at: { type: Sequelize.DATE },//延保整机开始日期
    keypart_begin_at: { type: Sequelize.DATE },//关键部位开始日期
    machine_end_at: { type: Sequelize.DATE },//延保整机结束日期
    keypart_end_at: { type: Sequelize.DATE },//关键部位结束日期
    ori_begin_at: { type: Sequelize.DATE },//发票购买日期
    ori_machine_end_at: { type: Sequelize.DATE },//整机原保结束日期
    ori_keypart_end_at: { type: Sequelize.DATE },//关键部位原保结束日期

    name:{ type: Sequelize.STRING(50), allowNull: true },//用户姓名
    tel:{ type: Sequelize.STRING(50), allowNull: true }, //联系方式
    model:{ type: Sequelize.STRING(50), allowNull: true }, //机器型号
    brand: { type: Sequelize.STRING(50), allowNull: true },//延保品牌
    category: { type: Sequelize.STRING(50), allowNull: true },//延保品类
    sub_category:{ type: Sequelize.STRING(50), allowNull: true },
    province: { type: Sequelize.INTEGER, allowNull: true },//省份
    city: { type: Sequelize.INTEGER, allowNull: true },//地市
    area: { type: Sequelize.INTEGER, allowNull: true }, //区县
    address:{ type: Sequelize.STRING(50), allowNull: true },//地址

    sale_name:{ type: Sequelize.STRING(50), allowNull: true },//销售员姓名
    sale_tel:{ type: Sequelize.STRING(50), allowNull: true },//销售员手机号

    out_trade_no:{ type: Sequelize.STRING(50), allowNull: true }, //商户订单号
    bank_type:{ type: Sequelize.STRING(50), allowNull: true },//付款银行类型
    transaction_id:{ type: Sequelize.STRING(50), allowNull: true },//订单号
    time_end:{ type: Sequelize.STRING(50), allowNull: true },//支付完成时间

    machine_life:{ type: Sequelize.STRING(50), allowNull: true },//整机厂保年限
    key_part_life:{ type: Sequelize.STRING(50), allowNull: true },//关键部位厂保年限

    //pic_filename:String//图片
    pic_filename: {type: Sequelize.TEXT, allowNull: true},
    dbid: { type: Sequelize.STRING(50), allowNull: true }
};

SCHEMA.service_axb = {
    id: { type: Sequelize.INTEGER, primaryKey: true, autoIncrement: true },
    tenant_id: { type: Sequelize.INTEGER, allowNull: false },
    department_id: { type: Sequelize.INTEGER, allowNull: false },
    product_id: { type: Sequelize.INTEGER, allowNull: false },
    customer_id: { type: Sequelize.STRING, allowNull: true },
    state: { type: Sequelize.STRING(11), allowNull: true },//Y有效
    type: { type: Sequelize.STRING(255), allowNull: true },//延保政策
    product_price: { type: Sequelize.INTEGER, allowNull: true },//商品价格
    price: { type: Sequelize.INTEGER, allowNull: true }, //延保费用
    extra: {type: Sequelize.TEXT, allowNull: true},
    deal_by: { type: Sequelize.INTEGER, allowNull: true }, //处理人
    opt_by: { type: Sequelize.INTEGER, allowNull: false },
    begin_at: { type: Sequelize.DATE },
    end_at: { type: Sequelize.DATE },
    life: { type: Sequelize.STRING(11), allowNull: true },//延保年限
    product_sn: { type: Sequelize.STRING(11), allowNull: true },//机器编码
    sn: { type: Sequelize.STRING(11), allowNull: true }, //对应axb的code
    invoice: { type: Sequelize.STRING(11), allowNull: true },
    comment: { type: Sequelize.STRING(11), allowNull: true },
    buy_at: Date,
    sell_at: Date,
    ori_begin_at: { type: Sequelize.DATE },//发票购买日期
    ori_end_at: { type: Sequelize.DATE },
    create_at: { type: Sequelize.DATE },
    update_at: { type: Sequelize.DATE },
    register_at: { type: Sequelize.DATE },

    name:{ type: Sequelize.STRING(50), allowNull: true },//用户姓名
    tel:{ type: Sequelize.STRING(50), allowNull: true }, //联系方式
    model:{ type: Sequelize.STRING(50), allowNull: true }, //机器型号
    brand: { type: Sequelize.STRING(50), allowNull: true },//延保品牌
    category: { type: Sequelize.STRING(50), allowNull: true },//延保品类
    sub_category:{ type: Sequelize.STRING(50), allowNull: true },
    province: { type: Sequelize.INTEGER, allowNull: true },//省份
    city: { type: Sequelize.INTEGER, allowNull: true },//地市
    area: { type: Sequelize.INTEGER, allowNull: true }, //区县
    address:{ type: Sequelize.STRING(50), allowNull: true },//地址

    sale_name:{ type: Sequelize.STRING(50), allowNull: true },//销售员姓名
    sale_tel:{ type: Sequelize.STRING(50), allowNull: true },//销售员手机号

    service_id:{ type: Sequelize.STRING(50), allowNull: true }, //商户订单号
    //铺货数据（需求新增录入字段）
    plain_code:{ type: Sequelize.STRING(50), allowNull: true },//明码
    terminal_code:{ type: Sequelize.STRING(50), allowNull: true },//网点代码（BP号）
    terminal_city:{ type: Sequelize.STRING(50), allowNull: true },//网点城市
    terminal_parent:{ type: Sequelize.STRING(50), allowNull: true },//网点上级
    terminal_description:{ type: Sequelize.STRING(50), allowNull: true },//网点描述
    Express_at:{ type: Sequelize.DATE },//延保卡寄送时间
    Express_Number:{ type: Sequelize.STRING(50), allowNull: true },//延保卡寄送快递单号
    //pic_filename://图片
    pic_filename: {type: Sequelize.TEXT, allowNull: true},
    product_type:{ type: Sequelize.STRING(50), allowNull: true },
    claim_amount:Number
};

SCHEMA.product = {
    id: { type: Sequelize.INTEGER, primaryKey: true, autoIncrement: true },
    tenant_id: { type: Sequelize.INTEGER, allowNull: false },
    name: { type: Sequelize.STRING(50), allowNull: false },
    model: { type: Sequelize.STRING(50), allowNull: true },
    brand: { type: Sequelize.STRING(50), allowNull: true },
    category: { type: Sequelize.STRING(50), allowNull: true },
    sub_category:{ type: Sequelize.STRING(50), allowNull: true },
    type: { type: Sequelize.STRING(50), allowNull: true },
    create_at: { type: Sequelize.DATE },
    update_at: { type: Sequelize.DATE }
};

SCHEMA.axb_card = {
    id: { type: Sequelize.INTEGER, primaryKey: true, autoIncrement: true },
    tenant_id: { type: Sequelize.INTEGER, allowNull: false },
    name: { type: Sequelize.STRING(50), allowNull: false },
    model: { type: Sequelize.STRING(50), allowNull: true },
    brand: { type: Sequelize.STRING(50), allowNull: true },
    category: { type: Sequelize.STRING(50), allowNull: true },
    sub_category:{ type: Sequelize.STRING(50), allowNull: true },
    type: { type: Sequelize.STRING(50), allowNull: true },
    create_at: { type: Sequelize.DATE },
    update_at: { type: Sequelize.DATE },

    code:  { type: Sequelize.STRING(50), allowNull: true },//延保注册码
    state: { type: Sequelize.STRING(50), allowNull: true },//延保注册码状态（0有效，未注册 1无效，已注册）
    registe_at:{ type: Sequelize.DATE },//注册日期

    //铺货数据（需求新增录入字段）
    plain_code: { type: Sequelize.STRING(50), allowNull: true },//明码
    department_id: { type: Sequelize.STRING(50), allowNull: true },//延保卡寄送的网点名称
    terminal_code: { type: Sequelize.STRING(50), allowNull: true },//网点代码（BP号）
    terminal_city: { type: Sequelize.STRING(50), allowNull: true },//网点城市
    terminal_parent: { type: Sequelize.STRING(50), allowNull: true },//网点上级
    terminal_description: { type: Sequelize.STRING(50), allowNull: true },//网点描述
    Express_at:{ type: Sequelize.DATE },//延保卡寄送时间
    Express_Number: { type: Sequelize.STRING(50), allowNull: true },//延保卡寄送快递单号

    //注册数据
    life: { type: Sequelize.STRING(50), allowNull: true },
    service_id: { type: Sequelize.STRING(50), allowNull: true }, //服务单号
    product_price: { type: Sequelize.DOUBLE, allowNull: true },//发票价格
    ori_begin_at: { type: Sequelize.DATE },//发票日期
    pic_filename: { type: Sequelize.TEXT },//上传截图
    tel: { type: Sequelize.STRING(50), allowNull: true }, //联系方式
    price: { type: Sequelize.DOUBLE, allowNull: true },//延保费用
    claim_amount:{ type: Sequelize.DOUBLE, allowNull: true },

    //收款数据
    receive_state: { type: Sequelize.STRING(50), allowNull: true },//收款状态（0：未收款，1已收款）
    picc_fee: { type: Sequelize.DOUBLE, allowNull: true },//PICC保费
    suning_commission: { type: Sequelize.DOUBLE, allowNull: true },//苏宁佣金
    fubang_profit: { type: Sequelize.DOUBLE, allowNull: true },//富邦毛利
    wholesale_price:{ type: Sequelize.DOUBLE, allowNull: true },//网点批发价格
    receive_at: { type: Sequelize.DATE },//收款日期
    picc_fee_at: { type: Sequelize.DATE },//保费支付日期
    suning_commission_at: { type: Sequelize.DATE },//佣金支付日期日期

    //发票信息
    invoice_state: { type: Sequelize.STRING(50), allowNull: true },//开票状态
    invoice_sn: { type: Sequelize.STRING(50), allowNull: true },//开票号
    invoice_price:{ type: Sequelize.DOUBLE, allowNull: true },//开票金额
    invoice_at:{ type: Sequelize.DATE },
    product_type: { type: Sequelize.STRING(50), allowNull: true }
};


exports.SCHEMA = SCHEMA;