var Promise = require('bluebird');
var _ = require('lodash');
var log4js = require('log4js');
var logger = log4js.getLogger('store');
var grc = require('../core').grc;
var rc = require('../core').rc;


/**
 * @param  {} id
 */
exports.getAgent = function(id){
    return grc.get(`agent_${id}`);
};

exports.setAgent = function(ads){    
    return grc.set(`agent_${ads.agent_id}`, ads);
};

exports.delAgent = function(id){    
    return grc.del(`agent_${id}`);
};
