/**
 * @fileOverview 上传api
 * @author wing ying_gong@bjdv.com
 */
'use strict';
var Promise = require('bluebird');
var fs = require('fs');
var path = require('path');
var logger = require('log4js').getLogger('upload');
var express = require('express');
var router = express.Router();

var multer = require('multer');
var UPLOADS_PATH = path.join(__dirname, '../../../uploads');
var RESOURCE_PATH = path.join(__dirname, '../../../resources');

module.exports = router;

router.get('/:type/:file', function (req, res) {
    var type = req.param('type');
    var file = req.param('file');
    var filePath = path.join(RESOURCE_PATH, type, file);
    if (fs.existsSync(filePath)) {
        res.sendFile(filePath);
    } else {
        res.status(404).send('文件不存在');
    }
});

var upload = multer({ dest: UPLOADS_PATH, limits: { fileSize: 50 * 1024 * 1024 } }).single('file');

router.post('/import', function (req, res) {
    upload(req, res, function (err) {
        var file = req.file;
        if (!err) {
            res.send(file.filename);
        } else {
            res.status(500).send(err.message);
        }
    });
});

router.post('/:type', function (req, res) {
    upload(req, res, function (err) {
        try {
            var type = req.param('type');
            var file = req.file;
            //logger.info(file);
            if (!err) {
                if (!type && req.user) {
                    res.status(500).send('路径类型错误');
                } else {
                    var tenantId = String(req.user.tenant_id);
                    //logger.info(req.user);
                    if (!fs.existsSync(path.join(RESOURCE_PATH, tenantId))) {
                        fs.mkdirSync(path.join(RESOURCE_PATH, tenantId));
                    }
                    if (!fs.existsSync(path.join(RESOURCE_PATH, tenantId, type))) {
                        fs.mkdirSync(path.join(RESOURCE_PATH, tenantId, type));
                    }
                    var oldPath = path.join(UPLOADS_PATH, file.filename);
                    var newPath = path.join(RESOURCE_PATH, tenantId, type, file.filename);
                    //logger.info(oldPath, newPath);
                    fs.renameSync(oldPath, newPath);
                    res.send(file.filename);
                }
            } else {
                res.status(500).send(err.message);
            }
        } catch (e) {
            res.status(500).send(e.message);
        }

    });
});

