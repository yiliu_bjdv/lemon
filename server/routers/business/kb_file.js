/**
 * @fileOverview 坐席api
 * @author wing ying_gong@bjdv.com
 */
var express = require('express');
var router = express.Router();

var Promise = require('bluebird');
var _ = require('lodash');
var logger = require('log4js').getLogger('kb_file');

module.exports = require('../restful')('kb_file', router);
