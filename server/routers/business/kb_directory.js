/**
 * @fileOverview 知识库api
 * @author wing ying_gong@bjdv.com
 */
var express = require('express');
var router = express.Router();

var Promise = require('bluebird');
var _ = require('lodash');
var logger = require('log4js').getLogger('kb_directory');
var error = require('../../error');

var tableName = 'kb_directory';

router.get('/', function (req, res) {
    var page = req.query.page;
    var limit = req.query.limit;
    var options = req.query.options;
    Promise.resolve().then(function () {
        if (_.isUndefined(limit)) {
            return req.dbl.findAll(tableName, options);
        } else {
            return req.dbl.findAllPage(tableName, options, page, limit);
        }
    }).then(function (data) {
        res.json({ result: data });
    }).catch(function (e) {
        res.json({ err: e.res || 800, result: e.message });
        logger.error(e.stack);
    });
});

router.get('/count', function (req, res) {
    var where = req.query.where;
    Promise.resolve().then(function () {
        return req.dbl.count(tableName, where);
    }).then(function (count) {
        res.json({ result: count });
    }).catch(function (e) {
        res.json({ err: e.res || 800, result: e.message });
        logger.error(e.stack);
    });
});

router.get('/son/:id', function (req, res) {
    var id = req.params.id;
    Promise.resolve().then(function () {
        if (_.isUndefined(id)) {
            throw error.new(error.RES.DEPARTMENT_PARENT_ERR);
        }
        if (id == 'null') {
            return req.dbl.findAll(tableName, { where: { parent: null } });
        } else {
            return req.dbl.findOne(tableName, id).then(function (row) {
                if (!row) {
                    throw error.new(error.RES.DEPARTMENT_PARENT_ERR);
                }
                return req.dbl.findAll(tableName, { where: { parent: id }, order: [['ancestors']] });
            });
        }
    }).then(function (rows) {
        res.json({ result: rows });
    }).catch(function (e) {
        res.json({ err: e.res || 800, result: e.message });
        logger.error(e.stack);
    });
});

router.get('/leaf/:id', function (req, res) {
    var id = req.params.id;
    Promise.resolve().then(function () {
        if (_.isUndefined(id)) {
            throw error.new(error.RES.DEPARTMENT_PARENT_ERR);
        }
        return req.dbl.findOne(tableName, id);
    }).then(function (row) {
        if (!row) {
            throw error.new(error.RES.DEPARTMENT_PARENT_ERR);
        }
        return req.dbl.findAll(tableName, { where: { ancestors: { $like: '%{' + id + '}%' } }, order: [['ancestors']] });
    }).then(function (rows) {
        res.json({ result: rows });
    }).catch(function (e) {
        res.json({ err: e.res || 800, result: e.message });
        logger.error(e.stack);
    });
});

router.get('/:id', function (req, res) {
    var id = req.params.id;
    var options = req.query.options;
    Promise.resolve().then(function () {
        return req.dbl.findOne(tableName, id, options);
    }).then(function (data) {
        res.json({ result: data });
    }).catch(function (e) {
        res.json({ err: e.res || 800, result: e.message });
        logger.error(e.stack);
    });
});

router.post('/', function (req, res) {
    var data = req.body.data;
    Promise.resolve().then(function () {
        data.ancestors = "";
        data.parent = null;
        data.depth = 0;
        return req.dbl.create(tableName, data);
    }).then(function () {
        res.json({ result: true });
    }).catch(function (e) {
        res.json({ err: e.res || 800, result: e.message });
        logger.error(e.stack);
    });
});

router.post('/:parent', function (req, res) {
    var parent = req.params.parent;
    var data = req.body.data;
    Promise.resolve().then(function () {
        if (_.isUndefined(parent)) {
            throw error.new(error.RES.DEPARTMENT_PARENT_ERR);
        } else {
            return req.dbl.findOne(tableName, parent, { attributes: ['id', 'depth', 'ancestors'] }).then(function (row) {
                if (!row) {
                    throw error.new(error.RES.DEPARTMENT_PARENT_ERR);
                }
                data.ancestors = row.ancestors || "" + '{' + row.id + '}';
                data.parent = row.id;
                data.depth = row.depth + 1;
            });
        }
    }).then(function () {
        return req.dbl.create(tableName, data);
    }).then(function () {
        res.json({ result: true });
    }).catch(function (e) {
        res.json({ err: e.res || 800, result: e.message });
        logger.error(e.stack);
    });
});

router.put('/:id', function (req, res) {
    var id = req.params.id;
    var data = req.body.data;
    Promise.resolve().then(function () {
        /* 有问题解决不了子孙的属性需要改.
        if (!data.parent) {
            data.ancestors = "";
            data.parent = null;
            data.depth = 0;
        } else {
            return req.dbl.findOne(tableName, data.parent, { attributes: ['ancestors'] }).then(function (row) {
                if (!row) {
                    throw error.new(error.RES.DEPARTMENT_PARENT_ERR);
                }
                if (row.ancestors.indexOf("{" + id + "}") >= 0) {
                    throw error.new(error.RES.DEPARTMENT_CANNOT_BELONG_SELF_SON);
                }
                data.ancestors = row.ancestors || "" + '{' + row.id + '}';
                data.parent = row.id;
                data.depth = row.depth + 1;
            });
        }*/
        data.ancestors = undefined;
        data.parent = undefined;
        data.depth = undefined;
    }).then(function () {
        return req.dbl.update(tableName, data, { id: id });
    }).then(function () {
        res.json({ result: true });
    }).catch(function (e) {
        res.json({ err: e.res || 800, result: e.message });
        logger.error(e.stack);
    });
});

router.delete('/:id', function (req, res) {
    var id = req.params.id;
    Promise.resolve().then(function () {
        if (!id) {
            throw error.new(error.RES.DEPARTMENT_ERR);
        }
        return req.dbl.count(tableName, {where: { parent: id }});
    }).then(function (count) {
        if (count > 0) {
            throw error.new(error.RES.DEPARTMENT_CANNOT_DELETE_HAS_SON);
        }
        return req.dbl.remove(tableName, id);
    }).then(function () {
        res.json({ result: true });
    }).catch(function (e) {
        res.json({ err: e.res || 800, result: e.message });
        logger.error(e.stack);
    });
});

module.exports = router;
