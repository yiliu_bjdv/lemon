/**
 * @fileoverview 工单分类api
 * @author wing ying_gong@bjdv.com
 */

module.exports = require('../restful')('service_category');