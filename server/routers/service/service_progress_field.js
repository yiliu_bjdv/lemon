/**
 * @fileoverview 工单进度表单项api
 * @author wing ying_gong@bjdv.com
 */

module.exports = require('../restful')('service_progress_field');