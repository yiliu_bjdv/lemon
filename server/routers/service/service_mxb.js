/**
 * @fileoverview 工单api
 * @author wing ying_gong@bjdv.com
 */

var express = require('express');
var router = express.Router();

var Promise = require('bluebird');
var _ = require('lodash');
var logger = require('log4js').getLogger('service_mxb');

router.get('/', function (req, res) {
    var page = req.query.page;
    var limit = req.query.limit;
    var options = req.query.options;
    Promise.resolve().then(function () {
        if (_.isUndefined(limit)) {
            return req.dbl.findAllPage('service_mxb',options);
        } else {
            return req.dbl.findAllPage('service_mxb',options, page, limit);
        }
    }).then(function (data) {
        res.json({ result: data });
    }).catch(function (e) {
        res.json({ err: e.cause || 800, result: e.message });
        logger.error(e.stack);
    });
});

router.get('/count', function (req, res) {
    var options = req.query.options;
    Promise.resolve().then(function () {
        return req.dbl.count('service_mxb', options);
    }).then(function (count) {
        res.json({ result: count });
    }).catch(function (e) {
        res.json({ err: e.cause || 800, result: e.message });
        logger.error(e.stack);
    });
});

router.get('/:id', function (req, res) {
    var id = req.params.id;
    var options = req.query.options;
    var service;
    Promise.resolve().then(function () {
        return req.dbl.findOne('service_mxb', id, options);
    }).then(function (row) {
        service = row;
        try {
            service.extra = JSON.parse(service.extra);
        } catch (e) {
            service.extra = {};
        }
        
        if (service.customer_id) {
            return req.dbl.findOne('customer', id).then(function (row) {
                service.customer = row;
            });
        }
    }).then(function () {
        res.json({ result: service });
    }).catch(function (e) {
        res.json({ err: e.cause || 800, result: e.message });
        logger.error(e.stack);
    });
});

router.post('/', function (req, res) {
    var data = req.body.data;
    Promise.resolve().then(function () {
        if (data.customer) {
            if (data.customer.id) {
                return req.dbl.update('customer', data.customer, { id: data.customer.id });
            } else {
                return req.dbl.create('customer', data.customer).then(function(row){
                    data.customer.id = row.id;
                });
            }
        }
    }).then(function () {
        data.customer_id = data.customer.id;
        data.extra = JSON.stringify(data.extra || {});
        return req.dbl.create('service', data);
    }).then(function () {
        res.json({ result: true });
    }).catch(function (e) {
        res.json({ err: e.cause || 800, result: e.message });
        logger.error(e.stack);
    });
});

router.put('/:id', function (req, res) {
    var id = req.params.id;
    var data = req.body.data;
    var date = req.body.date;
    Promise.resolve().then(function () {
        //记录日志和变更审计信息
        return req.dbl.saveAudit(data,'修改工单');
    }).then(function () {
        data.extra = JSON.stringify(data.extra || {});
        return req.dbl.update('service_mxb',  data, { id: id });
    }).then(function () {
        res.json({ result: true });
    }).catch(function (e) {
        res.json({ err: e.res || 800, result: e.message });
        logger.error(e.stack);
    });
});
router.delete('/:id', function (req, res) {
    var id = req.params.id;
    Promise.resolve().then(function () {
        return req.dbl.remove('service_mxb', id);
    }).then(function () {
        res.json({ result: true });
    }).catch(function (e) {
        res.json({ err: e.cause || 800, result: e.message });
        logger.error(e.stack);
    });
});

module.exports = router;
