/**
 * @fileoverview 工单分类进度api
 * @author wing ying_gong@bjdv.com
 */

var express = require('express');
var router = express.Router();

var Promise = require('bluebird');
var _ = require('lodash');
var logger = require('log4js').getLogger('service_category_progress');

router.get('/', function(req, res){
    var categoryId = req.query.category_id;
    var progressId = req.query.progress_id;
    Promise.resolve().then(function () {
        return req.dbl.findCategoryProgress({category_id: categoryId, progress_id: progressId});
    }).then(function (data) {
        res.json({ result: data });
    }).catch(function (e) {
        res.json({ err: e.cause || 800, result: e.message });
        logger.error(e.stack);
    });
});

router.post('/:category_id', function(req, res){
    var categoryId = req.params.category_id;
    var data = req.body.data;
    Promise.resolve().then(function(){
        return req.dbl.updateCategoryProgress(categoryId, data);
    }).then(function(){
        res.json({result: true});
    }).catch(function(e){
        res.json({ err: e.cause || 800, result: e.message });
        logger.error(e.stack);
    });
});


module.exports = router;