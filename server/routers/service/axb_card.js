/**
 * @fileOverview 客户api
 * @author wing ying_gong@bjdv.com
 */
var express = require('express');
var router = express.Router();

var Promise = require('bluebird');
var _ = require('lodash');
var logger = require('log4js').getLogger('axb_card');

module.exports = require('../restful')('axb_card', router);