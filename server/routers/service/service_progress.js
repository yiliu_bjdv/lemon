/**
 * @fileoverview 工单进度api
 * @author wing ying_gong@bjdv.com
 */

module.exports = require('../restful')('service_progress');