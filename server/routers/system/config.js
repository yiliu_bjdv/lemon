/**
 * @fileOverview 配置api
 * @author wing ying_gong@bjdv.com
 */
var logger = require('log4js').getLogger('config');
var express = require('express');
var router = express.Router();
var CATEGORIES = [
    {key: 'test', name: '测试配置'}
];
router.get('/category', function (req, res) {
    res.json({ result: CATEGORIES });
});
module.exports = require('../restful')('config', router);