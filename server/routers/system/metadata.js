var logger = require('log4js').getLogger('metadata');
var _ = require('lodash');
var express = require('express');
var router = express.Router();

var CATEGORIES = [
    {key: 'test', name: '测试', type: 'number'}
];

router.get('/category', function (req, res) {
    res.json({ result: CATEGORIES });
});

router.get('/category/:key', function (req, res) {
    var key = req.params.key;
    res.json({ result: _.find(CATEGORIES, { key: key }) });
});

router.get('/ivr', function (req, res) {
    Promise.resolve().then(() => {
        return req.dbl.findAll('ivr', {}, [['id']], ['id', 'name']);
    }).then((rows) => {
        rows.forEach(function (row) {
            row.key = String(row.key);
        });
        res.json({ result: rows });
    }).catch(function (err) {
        res.json({ err: err.message });
    });
});

router.get('/queue', function (req, res) {
    Promise.resolve().then(() => {
        return req.dbl.findAll('queue', {}, [['id']], ['id', 'name']);
    }).then((rows) => {
        rows.forEach(function (row) {
            row.key = String(row.key);
        });
        res.json({ result: rows });
    }).catch(function (err) {
        res.json({ err: err.message });
    });
});

router.get('/user', function (req, res) {
    Promise.resolve().then(() => {
        return req.dbl.findAll('user', {}, [['id']], ['id', 'name']);
    }).then((rows) => {
        rows.forEach(function (row) {
            row.key = String(row.key);
        });
        res.json({ result: rows });
    }).catch(function (err) {
        res.json({ err: err.message });
    });
});

module.exports = require('../restful')('metadata', router);