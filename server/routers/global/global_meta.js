var express = require('express');
var router = express.Router();
var _ = require('lodash');
var logger = require('log4js').getLogger('global_meta');

var CATEGORIES = [
    { key: 'status', name: '状态', type: 'number' },
    { key: 'service:state', name: '工单状态', type: 'number' },
    { key: 'service:field', name: '工单扩展类型' }, 
    { key: 'product:type', name: '产品类型' }, 
    { key: 'product:category', name: '产品类别' }, 
    { key: 'product:sub_category', name: '分段类目' }, 
    { key: 'service_axb:type', name: '安心保购买渠道' }, 
    { key: 'service_mxb:type', name: '美心保购买渠道' }, 
    { key: 'service_axb:life', name: '安心保年限' },   
    { key: 'service_mxb:life', name: '美心保年限' }, 
];

router.get('/category', function (req, res) {
    res.json({ result: CATEGORIES });
});

router.get('/category/:key', function (req, res) {
    var key = req.params.key;
    res.json({ result: _.find(CATEGORIES, { key: key }) });
});

module.exports = require('../restful')('global_meta', router);