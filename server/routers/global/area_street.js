var logger = require('log4js').getLogger('area_street');
var guard = require('express-jwt-permissions')();
var express = require('express');
var router = express.Router();
router.get('/', function(req, res){
    var code = req.query.code;
    req.dbl.findStreet(code).then(function(rows){
        res.json({result: rows});
    }).catch(function(e){
        res.json({err: e.res||800, result: e.message});
    });
});
module.exports = router;