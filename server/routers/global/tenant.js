/**
 * @fileOverview 坐席api
 * @author wing ying_gong@bjdv.com
 */
var express = require('express');
var router = express.Router();

var Promise = require('bluebird');
var _ = require('lodash');
var logger = require('log4js').getLogger('tenant');
var db = require('../../db');

router.post('/admin', (req, res) => {
    let admin = req.body.data;
    //logger.info('admin tenant', req.user);
    db.tenantAdmin(req.user.id, admin).then(() => {
        res.json({ result: true });
    }).catch((e) => {
        res.json({ err: e.cause || 800, result: e.message });
    });
});

module.exports = require('../restful')('tenant', router);
