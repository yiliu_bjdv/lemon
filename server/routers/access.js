/**
 * @fileOverview 认证api
 * @author wing ying_gong@bjdv.com
 */
var express = require('express');
var router = express.Router();
module.exports = router;
var jwt = require('jsonwebtoken');

var Promise = require('bluebird'), _ = require('lodash');
var logger = require('log4js').getLogger('access');
var error = require('../error').error;
var CAUSE = require('../error').CAUSE;
var db = require('../db');
router.post('/auth', function (req, res) {
    var userName = req.body.user_name;
    var password = req.body.password;
    var expire = req.body.expire || process.env.JWT_EXPIRE;
    var o;
    Promise.resolve().then(function () {
        return db.userAuth(userName);
    }).then(function (row) {
        if (!row) {
            throw error(CAUSE.USER_NOTEXIST);
        }
        if (row.status == 0) {
            throw error(CAUSE.USER_DISABLED);
        }
        if (row.password != password) {
            throw error(CAUSE.USER_PASSWORD_ERR);
        }
        var expireRegex = /^[1-9]\d{0,2}h$/;
        logger.info(expire);
        if (!expireRegex.test(expire)){
            throw error(CAUSE.USER_EXPIRE_ERR);
        }
        o = {
            id: row.id,
            tenant_id: row.tenant_id,
            user_name: row.user_name,
            name: row.name,
            level: row.level,
            tel: row.tel,
            department_id: row.department_id,
            create_at: row.create_at,
            update_at: row.update_at
        };
        return db.userPermission(row.level);
    }).then(function (rows) {
        o.permissions = _.map(rows, 'permission');
        var jwtSecret = process.env.JWT_SECRET;
        var jwtExpire = expire;
        var token = jwt.sign(o, jwtSecret, { expiresIn: jwtExpire });
        res.json({
            result: {
                user: o,
                token: token
            }
        });
    }).catch(function (e) {
        logger.warn(e.stack);
        res.json({ err: e.cause||800, result: e.message });
    });
});

router.post('/signup', function (req, res) {
    res.json({ err: '功能未开放' });
});

